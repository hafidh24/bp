import {
  Text, View, ScrollView, TextInput, StyleSheet,
  Dimensions, ToastAndroid, TouchableOpacity, Image, Alert
} from 'react-native'
import React, { Component } from 'react'
import * as ImagePicker from "react-native-image-picker"
import { Picker } from '@react-native-picker/picker';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { ADD } from '../../assets/icons'
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
export class tambah extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: this.props.route.params.item,
      jiwa: '',
      petugas: '',
      jamban: '',
      rumah: '',
      gaji: '',
      lantai: '',
      dinding: '',
      air: '',
      biaya: '',
      pekerjaan: '',
      elektronik: '',
      keterangan: '',
      lihatkeluarga: false,
      lihatasset: false,
      lihatekonomi: false,
      lihatibadah: false,
      lihatkesehatan: false,
      lihatlainya: false,
      lihathasil: false,
      rapimg: {
        0: {
          image: {
            ADD
          },
        },
      },
      img1: null,
      img2: 0,
    };
  }
  takePic(index) {
    {
      ImagePicker.launchCamera(
        {
          noData: true,
          saveToPhotos: true,
          title: 'Select Photo',
          maxWidth: 300,
          maxHeight: 400,
          allowsEditing: true,
          compressImageQuality: 0.5,
          storageOptions: {
            skipBackup: false,
            path: 'images',
          },
        },
        response => {
          console.log('Response = ', response);

          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else {
            const source = {
              image: {
                uri: response.assets[0].uri,
                name: response.assets[0].fileName,
                type: response.assets[0].type,
              },
              //   id: 0,
            };
            console.log('ini gambar = ', source);
            this.setState(
              prevState => {
                prevState.rapimg[index] = source;
                //   prevState.tgl_lahir[index] != this.props.route.params.tgl_lahir[index] ? this.setState({tglmbuh : true}) : this.setState({tglmbuh : false});
                return {
                  rapimg: prevState.rapimg,
                };
              },
              () => console.log(this.state.rapimg),
            );
            this.setState({
              img1: index,
              img2: index,
            });
            console.log('ini gambar = ', this.state.rapimg);
          }
        },
      );
    }
  }
  render() {
    var Pilihan = [
      { label: 'Ya', value: 'Ya' },
      { label: 'Tidak', value: 'Tidak' },
    ];
    var koran = [
      { label: 'Selalu', value: 'Selalu' },
      { label: 'Jarang', value: 'Jarang' },
      { label: 'Tidak Pernah', value: 'Tidak Pernah' },
    ];
    var majelis = [
      { label: 'Rutin', value: 'Rutin' },
      { label: 'Jarang', value: 'Jarang' },
      { label: 'Tidak Pernah', value: 'Tidak Pernah' },
    ];
    var Alquran = [
      { label: 'Lancar', value: 'Lancar' },
      { label: 'Terbata-bata', value: 'Terbata-bata' },
      { label: 'Tidak Bisa', value: 'Tidak Bisa' },
    ];
    var waktu = [
      { label: 'Lengkap', value: 'Lengkap' },
      { label: 'Kadang', value: 'Kadang-kadang' },
      { label: 'Tidak Pernah', value: 'Tidak Pernah' },
    ];
    var statusanak = [
      { label: 'Yatim', value: 'Yatim' },
      { label: 'Dhuafa', value: 'Dhuafa' },
      { label: 'Non Dhuafa', value: 'Non Dhuafa' },
    ];
    var Kendaraan = [
      { label: 'Sepeda', value: 'Sepeda' },
      { label: 'Motor', value: 'Motor' },
      { label: 'Mobil', value: 'Mobil' },
    ];
    var TPS = [
      { label: 'TPS', value: 'TPS' },
      { label: 'Sungai', value: 'Sungai' },
      { label: 'Pekarangan', value: 'Pekarangan' },
    ];
    var detail = this.state.detail
    var rapimg = [];
    for (let i = 0; i <= this.state.img2; i++) {
      rapimg.push(
        <Image
          style={{ width: 200, height: 200, resizeMode: 'contain', }}
          source={this.state.rapimg[i].image}
        />,
      );
    }
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <View>
          <Text style={{ marginLeft: 20, marginTop: 15, fontWeight: 'bold', fontSize: 20 }}>Data Survey</Text>
        </View>

        <View style={{ justifyContent: 'center', flexDirection: 'row', marginVertical: 10, }}>
          <TouchableOpacity style={[style.kotak, {
            marginHorizontal: 15, width: '25%',
            backgroundColor: this.state.lihatkeluarga === false ? '#fff' : '#0EBEDF',
            borderColor: this.state.lihatkeluarga === false ? '#bdbdbd' : '#fff',
            padding: 8, borderRadius: 5
          }]}
            onPress={() => this.setState({
              lihatkeluarga: !this.state.lihatkeluarga, lihathasil: false,
              lihatasset: false, lihatekonomi: false, lihatibadah: false, lihatkesehatan: false, lihatlainya: false
            })}>
            <Text style={{ color: this.state.lihatkeluarga === false ? '#bdbdbd' : '#fff', textAlign: 'center' }}>Keluarga</Text>
          </TouchableOpacity>

          <TouchableOpacity style={[style.kotak, {
            marginHorizontal: 15, width: '25%',
            backgroundColor: this.state.lihatasset === false ? '#fff' : '#0EBEDF',
            borderColor: this.state.lihatasset === false ? '#bdbdbd' : '#fff',
            padding: 8, borderRadius: 5
          }]} onPress={() => this.setState({
            lihatkeluarga: false, lihathasil: false,
            lihatasset: !this.state.lihatasset, lihatekonomi: false, lihatibadah: false, lihatkesehatan: false, lihatlainya: false
          })}>
            <Text style={{ color: this.state.lihatasset === false ? '#bdbdbd' : '#fff', textAlign: 'center' }}>Asset</Text>
          </TouchableOpacity>

          <TouchableOpacity style={[style.kotak, {
            marginHorizontal: 15, width: '25%',
            backgroundColor: this.state.lihatkesehatan === false ? '#fff' : '#0EBEDF',
            borderColor: this.state.lihatkesehatan === false ? '#bdbdbd' : '#fff',
            padding: 8, borderRadius: 5
          }]} onPress={() => this.setState({
            lihatkeluarga: false, lihathasil: false,
            lihatasset: false, lihatekonomi: false, lihatibadah: false, lihatkesehatan: !this.state.lihatkesehatan, lihatlainya: false
          })}>
            <Text style={{ color: this.state.lihatkesehatan === false ? '#bdbdbd' : '#fff', textAlign: 'center' }}>Kesehatan</Text>
          </TouchableOpacity>
        </View>

        <View style={{ justifyContent: 'center', flexDirection: 'row', }}>
          <TouchableOpacity style={[style.kotak, {
            marginHorizontal: 15, width: '25%',
            backgroundColor: this.state.lihatibadah === false ? '#fff' : '#0EBEDF',
            borderColor: this.state.lihatibadah === false ? '#bdbdbd' : '#fff',
            padding: 8, borderRadius: 5
          }]} onPress={() => this.setState({
            lihatkeluarga: false, lihathasil: false,
            lihatasset: false, lihatekonomi: false, lihatibadah: !this.state.lihatibadah, lihatkesehatan: false, lihatlainya: false
          })}>
            <Text style={{ color: this.state.lihatibadah === false ? '#bdbdbd' : '#fff', textAlign: 'center' }}>Ibadah</Text>
          </TouchableOpacity>


          <TouchableOpacity style={[style.kotak, {
            marginHorizontal: 15, width: '25%',
            backgroundColor: this.state.lihatlainya === false ? '#fff' : '#0EBEDF',
            borderColor: this.state.lihatlainya === false ? '#bdbdbd' : '#fff',
            padding: 8, borderRadius: 5
          }]} onPress={() => this.setState({
            lihatkeluarga: false, lihathasil: false,
            lihatasset: false, lihatekonomi: false, lihatibadah: false, lihatkesehatan: false, lihatlainya: !this.state.lihatlainya,
          })}>
            <Text style={{ color: this.state.lihatlainya === false ? '#bdbdbd' : '#fff', textAlign: 'center' }}>Lainya</Text>
          </TouchableOpacity>

          <TouchableOpacity style={[style.kotak, {
            marginHorizontal: 15, width: '25%',
            backgroundColor: this.state.lihathasil === false ? '#fff' : '#0EBEDF',
            borderColor: this.state.lihathasil === false ? '#bdbdbd' : '#fff',
            padding: 8, borderRadius: 5
          }]} onPress={() => this.setState({
            lihatkeluarga: false, lihathasil: !this.state.lihathasil,
            lihatasset: false, lihatekonomi: false, lihatibadah: false, lihatkesehatan: false, lihatlainya: false,
          })}>
            <Text style={{ color: this.state.lihathasil === false ? '#bdbdbd' : '#fff', textAlign: 'center' }}>Hasil</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={{ flexDirection: 'column', marginTop: 10 }}>
            {this.state.lihatkeluarga === true ?
              <View style={style.kotak}>
                <Text style={style.labelatas}>Keluarga</Text>
                <View style={{ justifyContent: 'center' }}>
                  <View style={style.form}>
                    <Text style={style.labelkiri}>No. Kartu Keluarga</Text>
                    <Text style={style.labelkk}>:{detail.no_kk} a/n {detail.kepala_keluarga}</Text>
                  </View>
                  <View style={style.form}>
                    <Text style={style.labelkiri}>Pendidikan Terakhir Kepala Keluarga</Text>
                    <View style={style.labelkanan2}>
                      <Picker
                        selectedValue={this.state.pen}
                        onValueChange={(itemValue) => {
                          this.setState({
                            pen: itemValue
                          })
                        }}>
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Pilih"
                          value=""
                          key={'0'}
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Tidak Sekolah"
                          value="Tidak Sekolah"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Sekolah Dasar"
                          value="Sekolah Dasar"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="SMP/MTS/Sederajat"
                          value="SMP/MTS/Sederajat"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Bimbel"
                          value="Bimbel"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="SMA/MA/SMK/Sederajat"
                          value="SMA/MA/SMK/Sederajat"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Diploma I"
                          value="Diploma I"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Diploma II"
                          value="Diploma II"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Diploma III"
                          value="Diploma III"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Diploma IV"
                          value="Diploma IV"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Strata-1"
                          value="Strata-1"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Strata-2"
                          value="Strata-2"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Strata-3"
                          value="Strata-3"
                        />
                        <Picker.Item
                          style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                          label="Lain-Lain"
                          value="Lain-Lain"
                        />

                      </Picker>

                    </View>
                  </View>
                  <View style={style.form}>
                    <Text style={style.labelkiri}>Jumlah Tanggungan Kepala Keluarga</Text>
                    <TextInput
                      style={style.labelkanan3}
                      onChangeText={jiwa => this.setState({ jiwa })}
                      value={this.state.jiwa}
                      keyboardType='default'
                      placeholder={''}
                      placeholderTextColor='#7e7e7e'
                    />
                  </View>
                </View>
              </View>
              : <View />
            }
            {this.state.lihatekonomi === true ?
              <View style={style.kotak}>
                <Text style={style.labelatas}>Ekonomi</Text>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Pekerjaan Kepala Keluarga</Text>
                  <View style={style.labelkanan2}>
                    <Picker
                      selectedValue={this.state.pen}
                      onValueChange={(itemValue) => {
                        this.setState({
                          pen: itemValue
                        })
                      }}>
                      <Picker.Item
                        style={{ fontSize: 12, fontFamily: 'Poppins-Regular' }}
                        label="Pilih"
                        value=""

                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Petani"
                        value="Petani"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Nelayan"
                        value="Nelayan"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Peternak"
                        value="Peternak"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Non PNS Dosen/Guru"
                        value="Non PNS Dosen/Guru"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="SMA/MA/SMK/Sederajat"
                        value="SMA/MA/SMK/Sederajat"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Guru PNS"
                        value="Guru PNS"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Guru Non PNS"
                        value="Guru Non PNS"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="PNS/TNI/POLRI"
                        value="PNS/TNI/POLRI"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Karyawan Swasta"
                        value="Karyawan Swasta"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Buruh"
                        value="Buruh"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Wiraswasta"
                        value="Wiraswasta"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Wirausaha"
                        value="Wirausaha"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Pedagang Kecil"
                        value="Pedagang Kecil"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Pedagang Besar"
                        value="Pedagang Besar"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Tidak Bekerja"
                        value="Tidak Bekerja"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Pensiunan"
                        value="Pensiunan"
                      />
                      <Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Sudah Meninggal"
                        value="Sudah Meninggal"
                      /><Picker.Item
                        style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                        label="Lainnya"
                        value="Lainnya"
                      />

                    </Picker>
                  </View>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Rata-rata Penghasilan Perbulan Kepala Keluarga</Text>
                  <View style={style.labelkanan2}>
                    <Picker
                      selectedValue={this.state.gaji}
                      onValueChange={(itemValue) => {
                        this.setState({
                          gaji: itemValue
                        })
                      }}>
                      <Picker.Item style={{ fontSize: 10 }} label="Pilih"
                        value="" />
                      <Picker.Item label="Dibawah Rp.500.000,-" value="0" />
                      <Picker.Item label="Rp.500.000,- s/d Rp.1.500.000,-" value="1" />
                      <Picker.Item label="Rp.1.500.000,- s/d Rp.2.500.000,-" value="2" />
                      <Picker.Item label="Rp.2.500.000,- s/d Rp.3.500.000,-" value="3" />
                      <Picker.Item label="Rp.3.500.000,- s/d Rp.5.000.000,-" value="4" />
                      <Picker.Item label="Rp.5.000.000,- s/d Rp.7.000.000,-" value="5" />
                      <Picker.Item label="Rp.7.000.000,- s/d Rp.10.000.000,-" value="6" />
                      <Picker.Item label="Diatas Rp.10.000.000,-" value="7" />
                    </Picker>
                  </View>
                </View>




                <View style={style.form}>
                  <Text style={style.labelkiri}>Kepemilikan Tabungan</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    formHorizontal={true}
                    buttonSize={10}
                    buttonOuterSize={19}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Makan 2x atau Lebih</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    formHorizontal={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}>
                  </RadioForm>
                </View>
              </View>
              : <View />
            }
            {this.state.lihatasset === true ?
              <View style={style.kotak}>
                <Text style={style.labelatas}>Asset</Text>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Kepemilikan Tanah</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Kepemilikan Rumah</Text>
                  <View style={style.labelkanan2}>
                    <Picker
                      selectedValue={this.state.rumah}
                      onValueChange={(itemValue) => {
                        this.setState({
                          rumah: itemValue
                        })
                      }}>
                      <Picker.Item style={{ fontSize: 10 }} l label="Pilih"
                        value="" />
                      <Picker.Item label="Hak Milik" value="Hak Milik" />
                      <Picker.Item label="Sewa" value="Sewa" />
                      <Picker.Item label="Orang Tua" value="Orang Tua" />
                      <Picker.Item label="Saudara" value="Saudara" />
                      <Picker.Item label="Kerabat" value="Kerabat" />
                    </Picker>
                  </View>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Kondisi Rumah</Text>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Lantai</Text>
                  <View style={style.labelkanan2}>
                    <Picker
                      selectedValue={this.state.lantai}
                      onValueChange={(itemValue) => {
                        this.setState({
                          lantai: itemValue
                        })
                      }}>
                      <Picker.Item style={{ fontSize: 10 }} l label="Pilih"
                        value="" />
                      <Picker.Item label="Keramik" value="Keramik" />
                      <Picker.Item label="Ubin" value="Ubin" />
                      <Picker.Item label="Marmer" value="Marmer" />
                      <Picker.Item label="Kayu" value="Kayu" />
                      <Picker.Item label="Tanah" value="Tanah" />
                      <Picker.Item label="Lainya" value="Lainya" />
                    </Picker>
                  </View>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Dinding</Text>
                  <View style={style.labelkanan2}>
                    <Picker
                      selectedValue={this.state.dinding}
                      onValueChange={(itemValue) => {
                        this.setState({
                          dinding: itemValue
                        })
                      }}>
                      <Picker.Item style={{ fontSize: 10 }} label="Pilih"
                        value="" />
                      <Picker.Item label="Tembok" value="Tembok" />
                      <Picker.Item label="Kayu" value="Kayu" />
                      <Picker.Item label="Papan" value="Papan" />
                      <Picker.Item label="Geribik" value="Geribik" />
                      <Picker.Item label="Lainnya" value="Lainnya" />
                    </Picker>
                    
                  </View>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Kepemilikan Kendaraan</Text>
                  <Text style={style.labelkanan}>:</Text>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Kepemilikan Eletronik</Text>
                  <TextInput
                    style={style.labelkanan3}
                    onChangeText={elektronik => this.setState({ elektronik })}
                    value={this.state.elektronik}
                    keyboardType='default'
                    placeholder={''}
                    placeholderTextColor='#7e7e7e'
                  />
                </View>
                <ScrollView horizontal={true} style={style.body}>
                  {rapimg}
                </ScrollView>
                <View style={{
                  zIndex: 0, width: '90%', paddingBottom: 40, paddingLeft: 30, paddingRight: 30,
                  justifyContent: 'center', alignItems: 'center'
                }}>
                  <TouchableOpacity onPress={() =>
                    Alert.alert(
                      'Info',
                      'Ambl foto menggunakan',
                      [
                        {
                          text: 'Kamera',
                          onPress: () =>
                            this.takePic(this.state.img1 === null ? 0 : this.state.img1 + 1),
                          style: 'cancel',
                        },
                      ],
                      { cancelable: true },
                    )
                  }
                  >
                    <View style={{ backgroundColor: '#00A9B8', borderRadius: 10, marginTop: 10, padding: 10 }}>
                      <Text style={{ color: '#fff', fontFamily: 'Poppins-Medium', fontSize: 13 }}>Ambil Foto</Text>
                    </View>
                  </TouchableOpacity>
                </View>

              </View>
              : <View />
            }
            {this.state.lihatkesehatan === true ?
              <View style={style.kotak}>
                <Text style={style.labelatas}>Kesehatan</Text>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Sumber Air Besih</Text>
                  <View style={style.labelkanan2}>
                    <Picker
                      selectedValue={this.state.air}
                      onValueChange={(itemValue) => {
                        this.setState({
                          air: itemValue
                        })
                      }}>
                      <Picker.Item style={{ fontSize: 10 }} label="Pilih"
                        value="" />
                      <Picker.Item label="Sumur" value="Sumur" />
                      <Picker.Item label="Sungai" value="Sungai" />
                      <Picker.Item label="PDAM" value="PDAM" />
                      <Picker.Item label="Lainnya" value="Lainnya" />
                    </Picker>
                  </View>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Jamban dan Saluran Limbah</Text>
                  <View style={style.labelkanan2}>
                    <Picker
                      selectedValue={this.state.jamban}
                      onValueChange={(itemValue) => {
                        this.setState({
                          jamban: itemValue
                        })
                      }}>
                      <Picker.Item style={{ fontSize: 10 }} label="Pilih"
                        value="" />
                      <Picker.Item label="Sungai" value="Sungai" />
                      <Picker.Item label="Septiktank" value="Septiktank" />
                      <Picker.Item label="Lainnya" value="Lainnya" />
                    </Picker>
                  </View>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Tempat Pembuangan Sampah</Text>
                  <RadioForm
                    radio_props={TPS}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Terdapat Perokok</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Terdapat Konsumen Miras</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Terdapat Persediaan Obat P3K</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Makan Buah dan Sayur Setiap Hari</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
              </View>
              : <View />
            }
            {this.state.lihatibadah === true ?
              <View style={style.kotak}>
                <Text style={style.labelatas}>Ibadah & Sosial</Text>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Solat 5 Waktu</Text>
                  <RadioForm
                    radio_props={waktu}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Membaca Alquran</Text>
                  <RadioForm
                    radio_props={Alquran}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Majelis Taklim</Text>
                  <RadioForm
                    radio_props={majelis}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Membaca Koran</Text>
                  <RadioForm
                    radio_props={koran}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Pengurus Organisasi</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
              </View>
              : <View />
            }
            {this.state.lihatlainya === true ?
              <View style={style.kotak}>
                <Text style={style.labelatas}>Lainya</Text>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Status Anak</Text>
                  <RadioForm
                    radio_props={statusanak}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Biaya Pendidikan Anak/bulan</Text>
                  <TextInput
                    style={style.labelkanan3}
                    onChangeText={biaya => this.setState({ biaya })}
                    value={this.state.biaya.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                    keyboardType='numeric'
                    placeholder={'Rp.'.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                    placeholderTextColor='#7e7e7e'
                  />
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Bantuan Rutin dari Lembaga Formal Lainnya</Text>
                  <RadioForm
                    radio_props={Pilihan}
                    onPress={value => {
                      ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                    }}
                    initial={-1}
                    animation={true}
                    buttonOuterSize={19}
                    buttonSize={10}
                    labelStyle={{ fontSize: 10, marginLeft: -5, paddingRight: 10 }}
                    formHorizontal={true}>
                  </RadioForm>
                </View>
              </View>
              : <View />
            }
            {this.state.lihathasil === true ?
              <View style={style.kotak}>
                <Text style={style.labelatas}>Data Survey</Text>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Resume Diskripstif, Kondisi Calon Penerima Manfaat</Text>
                  <TextInput
                    style={style.labelkanan4}
                    onChangeText={keterangan => this.setState({ keterangan })}
                    value={this.state.keterangan}
                    keyboardType='default'
                    placeholder={''}
                    placeholderTextColor='#7e7e7e'
                  />
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Petugas Survey</Text>
                  <TextInput
                    style={style.labelkanan3}
                    onChangeText={petugas => this.setState({ petugas })}
                    value={this.state.petugas}
                    keyboardType='default'
                    placeholder={''}
                    placeholderTextColor='#7e7e7e'
                  />
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Hasil Survey</Text>
                  <Text style={style.labelkanan}></Text>
                </View>
                <View style={style.form}>
                  <Text style={style.labelkiri}>Keterangan Survey</Text>
                  <TextInput
                    style={style.labelkanan4}
                    multiline={true}
                    onChangeText={keterangan => this.setState({ keterangan })}
                    value={this.state.keterangan}
                    keyboardType='default'
                    editable={false}
                    placeholder={''}
                    placeholderTextColor='#7e7e7e'
                  />
                </View>
              </View>
              : <View />
            }
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', marginTop: 20, }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('DataValidasi')} style={style.refresh} >
              <View style={{
                backgroundColor: '#0EBEDF', width: 100, height: 40, justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 10
              }}>
                <Text style={{ color: '#fff', textAlign: 'center', justifyContent: 'center', alignContent: 'center' }}>Kembali</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('')} style={style.refresh} >
              <View style={{
                backgroundColor: '#0EBEDF', width: 100, height: 40, justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 10
              }}>
                <Text style={{ color: '#fff', textAlign: 'center', justifyContent: 'center', alignContent: 'center' }}>Reset </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('')} style={style.refresh} >
              <View style={{
                backgroundColor: '#0EBEDF', width: 100, height: 40, justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 10
              }}>
                <Text style={{ color: '#fff', textAlign: 'center', justifyContent: 'center', alignContent: 'center' }}>Simpan</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}


export default tambah
const style = StyleSheet.create({
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    width: '100%'
  },
  labelkiri: {
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 5,
    marginLeft: 20,
    width: 100,
  },
  labelkanan: {
    fontSize: 12,
    marginHorizontal: 5,
    width: 150,
  },
  labelkk: {
    fontSize: 12,
    marginHorizontal: 5,
    width: 200,
  },
  labelkanan2: {
    fontSize: 12,
    marginHorizontal: 5,
    width: 220,
  },
  labelkanan3: {
    marginHorizontal: 5,
    width: 200,
    height: 40,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    justifyContent: 'center',
    alignContent: 'center'
  },
  labelkanan4: {
    marginHorizontal: 5,
    width: 200,
    height: 90,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    justifyContent: 'center',
    alignContent: 'center'
  },
  kotak: {
    backgroundColor: '#fff',
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 15,
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderColor: '#7e7e7e',
  },
  labelatas: {
    fontSize: 14,
    marginLeft: 20,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  Textinputcss: {
    width: windowWidth - 40,
    color: '#C0C0C0',
    marginTop: -10,
    borderRadius: 10,
    borderWidth: 1,
    fontSize: 10,
    height: 40,
    borderColor: '#C0C0C0',
    fontFamily: 'Poppins-Regular',
  },
  refresh: {
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
    // marginHorizontal: 5,
    bottom: 10,
  },//vildan
  body: {
    backgroundColor: '#EEEEEE',
    borderColor: '#CECBCB',
    borderRadius: 10,
    borderWidth: 3,
    borderStyle: 'dashed',
    backgroundColor: '#ffff',
    marginTop: 10,
    width: '90%',
    height: 200,
    marginBottom: 10,
    alignSelf: 'center',
    padding: 5
  },
})