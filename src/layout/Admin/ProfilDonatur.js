import {
  Text, View, SafeAreaView, ScrollView, ImageBackground, Image,
  StyleSheet, TouchableOpacity, Dimensions, TextInput
} from 'react-native'
import { background1, test } from '../../assets/images'
import React, { Component } from 'react'
import { Sekolah, Tgl, Jenis, } from '../../assets/icons';
import { Picker } from '@react-native-picker/picker';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
export class ProfilDonatur extends Component {
  constructor(props) {
    super(props)
    this.state = {
      det: [],
      kacab: [],
      wilbin: [],
      shelter:[],
      detail: this.props.route.params.item,
      namabaru: '',
      emailbaru: '',
      statusbaru: '',
      hpbaru: '',
      bankbaru: '',
      norekbaru: '',
      diprut: '',
      cab: '',
      wb: '',
      sh: '',
      alamat: ''
    }
  }
  GetShelterAPi() {
    fetch('https://kilauindonesia.org/datakilau/api/shelter/' + this.state.wb).then(res => {
      if (res.status === 200)
        return res.json()
    }).then(resdata => {
      console.log(resdata.data)
      this.setState({
        shelter: resdata.data,
      })
    })
  }
  GetWilbinAPi() {
    fetch('https://kilauindonesia.org/datakilau/api/wilbin/' + this.state.cab).then(res => {
      if (res.status === 200)
        return res.json()
    }).then(resdata => {
      console.log(resdata.data)
      this.setState({
        wilbin: resdata.data,
      })
    })
  }
  GetKacabAPi() {
    fetch('https://kilauindonesia.org/datakilau/api/kacab').then(res => {
      if (res.status === 200)
        return res.json()
    }).then(resdata => {
      console.log(resdata.data)
      this.setState({
        kacab: resdata.data,
      })
    })
  }
  componentDidMount() {
    this.GetKacabAPi();
    this.GetWilbinAPi();
    this.GetShelterAPi();
    console.log(this.props);
  }
  render() {
    var detail = this.state.detail
    return (
      <ScrollView style={{ backgroundColor: '#fff' }}>
        <ImageBackground source={background1} style={{ width: '100%', height: 160, }}>
        </ImageBackground>
        <View style={style.kolomkecil}>
          <Image source={test} style={{ width: 150, height: 150, borderRadius: 70, justifyContent: 'center', alignSelf: 'center', marginTop: -90, position: 'absolute' }}></Image>
          <View style={{ justifyContent: 'center', textAlign: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center', marginTop: 70 }}>
            <Text style={{ color: '#fff', fontSize: 16, fontWeight: 'bold', }}>{detail.full_name}</Text>
            {/* <View style={{ flexDirection: 'column', justifyContent: 'center', textAlign: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center', }}>
                          <View style={{ flexDirection: 'row' }}>
                              <Locations />
                              <Text style={style.labeldlm}>{detail.nama_shelter}</Text>
                          </View>
                      </View> */}
          </View>
          <View style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 15, padding: 5, }}>
              <View style={{ flexDirection: 'column', marginLeft: 20, marginTop: -5, justifyContent: 'center' }}>
                <Jenis style={{ justifyContent: 'center', alignSelf: 'center' }} />
                <Text style={{ marginTop: 2, color: '#fff', fontSize: 10 }}>Status</Text>
              </View>
              <View style={{ marginTop: -10, width: 1, height: '100%', backgroundColor: '#EBEAEA', }} />

              <View style={{ flexDirection: 'column', marginTop: -5, justifyContent: 'center' }}>
                <Tgl style={{ justifyContent: 'center', alignSelf: 'center' }} />
                <Text style={{ color: '#fff', fontSize: 10 }}>Diperuntukkan</Text>
              </View>
              <View style={{ marginTop: -10, width: 1, height: '100%', backgroundColor: '#EBEAEA', }} />

              <View style={{ flexDirection: 'column', marginTop: -5, justifyContent: 'center' }}>
                < Sekolah style={{ justifyContent: 'center', alignSelf: 'center' }} />
                <Text style={{ marginLeft: 3, color: '#fff', fontSize: 10 }}>Shelter</Text>
              </View>

            </View>

          </View>


        </View>
        <View style={{ paddingHorizontal: 20, backgroundColor: '#fff', width: '100%', marginTop: 10, }}>
          <View style={style.form}>
            <Text style={style.labelkiri}>Nama Lengkap</Text>
            <TextInput
              style={style.kotak3}
              onChangeText={namabaru => this.setState({ namabaru })}
              value={this.state.namabaru}
              keyboardType='default'
              placeholder={detail.full_name}
              placeholderTextColor="#C0C0C0"
            />
          </View>
          <View style={style.form}>
            <Text style={style.labelkiri}>Email</Text>
            <TextInput
              style={style.kotak3}
              onChangeText={emailbaru => this.setState({ emailbaru })}
              value={this.state.emailbaru}
              keyboardType='default'
              placeholder={detail.email}
              placeholderTextColor="#C0C0C0"
            />
          </View>
          <View style={style.form}>
            <Text style={style.labelkiri}>Status</Text>
            <TextInput
              style={style.kotak3}
              onChangeText={statusbaru => this.setState({ statusbaru })}
              value={this.state.statusbaru}
              keyboardType='default'
              placeholder={detail.status}
              placeholderTextColor="#C0C0C0"
            />
          </View>
          <View style={style.form}>
            <Text style={style.labelkiri}>No HP</Text>
            <TextInput
              style={style.kotak3}
              onChangeText={hpbaru => this.setState({ hpbaru })}
              value={this.state.hpbaru}
              keyboardType='default'
              placeholder={detail.no_hp}
              placeholderTextColor="#C0C0C0"
            />
          </View>
          <View style={style.form}>
            <Text style={style.labelkiri}>Bank</Text>
            <TextInput
              style={style.kotak3}
              onChangeText={bankbaru => this.setState({ bankbaru })}
              value={this.state.bankbaru}
              keyboardType='default'
              placeholder={detail.nama_bank}
              placeholderTextColor="#C0C0C0"
            />
          </View>
          <View style={style.form}>
            <Text style={style.labelkiri}>No Rekening</Text>
            <TextInput
              style={style.kotak3}
              onChangeText={norekbaru => this.setState({ norekbaru })}
              value={this.state.norekbaru}
              keyboardType='default'
              placeholder={detail.no_rek}
              placeholderTextColor="#C0C0C0"
            />
          </View>
          <View style={style.form}>
            <Text style={style.labelkiri}>Diperuntukkan</Text>
            <TextInput
              style={style.kotak3}
              onChangeText={diprut => this.setState({ diprut })}
              value={this.state.diprut}
              keyboardType='default'
              placeholder={detail.no_rek}
              placeholderTextColor="#C0C0C0"
            />
          </View>
          <View style={style.form}>
            <Text style={style.labelkiri}>Kantor Cabang</Text>
            <View style={style.kotakpicker}>
              <Picker
                style={style.Textinputcss}
                selectedValue={this.state.cab}
                onValueChange={itemValue =>
                  this.setState({ cab: itemValue, show: 1 })
                }>
                <Picker.Item
                  style={{ fontSize: 14, fontFamily: 'Poppins-Regular', fontWeight: 'bold' }}
                  label={detail.kacab}
                  value=""
                />
                {
                  this.state.kacab.map((cab) =>
                    <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={cab.nama_kacab.toString()} value={cab.id_kacab} key={cab.id_kacab} />
                  )}

              </Picker>
            </View>
          </View>

          <View style={style.form}>
            <Text style={style.labelkiri}>Wilayah Binaan</Text>
            <View style={style.kotakpicker}>
              <Picker
                style={style.Textinputcss}
                onFocus={() => { this.GetWilbinAPi() }}
                selectedValue={this.state.wb}
                onValueChange={(itemValue, kacab) => {
                  this.setState({
                    wb: (itemValue),
                    show: 1
                  })
                }}>
                <Picker.Item
                  style={{ fontSize: 14, fontFamily: 'Poppins-Regular', fontWeight: 'bold' }}
                  label={ detail.nama_wilbin }
                  value=""
                />
                {
                  this.state.wilbin.map((wb) =>
                    <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={wb.nama_wilbin.toString()} value={wb.id_wilbin} key={wb.id_wilbin} />
                  )}

              </Picker>
            </View>
          </View>

          <View style={style.form}>
            <Text style={style.labelkiri}>Shelter</Text>
            <View style={style.kotakpicker}>
            <Picker
                style={style.Textinputcss}
                onFocus={() => { this.GetShelterAPi() }}
                selectedValue={this.state.sh}
                onValueChange={(itemValue, wilbin) => {
                  this.setState({
                    sh: (itemValue),
                    show: 1
                  })
                }}>
                <Picker.Item
                  style={{ fontSize: 14, fontFamily: 'Poppins-Regular', fontWeight: 'bold' }}
                  label={detail.nama_shelter}
                  value=""
                />
                {
                  this.state.shelter.map((sh) =>
                    <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={sh.nama_shelter.toString()} value={sh.id_shelter} key={sh.id_shelter} />
                  )}
              </Picker>
            </View>
          </View>
          
          <View style={style.form}>
            <Text style={style.labelkiri}>Alamat</Text>
            <TextInput
              style={[style.kotak3, { height: 70 }]}
              onChangeText={alamat => this.setState({ alamat })}
              value={this.state.alamat}
              keyboardType='default'
              placeholder={detail.alamat}
              placeholderTextColor="#C0C0C0"
            />
          </View>

        </View>
        <View style={style.BSimpan2}>
          <TouchableOpacity>
            <View style={style.BSimpan}>
              <Text style={style.label5}>Edit</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

export default ProfilDonatur
const style = StyleSheet.create({
  labelkiri: {
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 5,
    marginLeft: 20,
    width: 100,
  },
  labelkanan: {
    fontSize: 12,
    marginHorizontal: 5,
    width: 150,
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    width: '100%'
  },
  kolomkecil: {
    backgroundColor: '#00A9B8',
    width: '100%',
  },
  BSimpan2: {
    height: 70,
    width: windowWidth,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  BSimpan: {
    backgroundColor: '#00A9B8',
    borderRadius: 10,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  label5: {
    color: '#fff',
    padding: 10,
    fontFamily: 'Poppins-Medium',
    fontSize: 13,
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  kotak3: {
    color: '#000000',
    borderColor: '#bdbdbd',
    margin: 10,
    borderRadius: 2,
    borderWidth: 1,
    fontSize: 12,
    height: 40,
    width: windowWidth * 0.5
  },
  kotakpicker: {
    marginTop: 10,
    marginLeft: 10,
    paddingVertical: 5,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#DDD',
    width: windowWidth * 0.5,
    height: 40,
  },
  Textinputcss: {
    width: windowWidth * 0.5,
    marginLeft: 10,
    color: '#C0C0C0',
    marginTop: -10,
    borderRadius: 10,
    borderWidth: 1,
    fontSize: 10,
    height: 40,
    borderColor: '#C0C0C0',
    fontFamily: 'Poppins-Regular',
  },
});