import {
    Text, View, SafeAreaView, TextInput, TouchableOpacity,
    StyleSheet, FlatList, RefreshControl, Modal, Image, ToastAndroid, ScrollView
} from 'react-native'
import React, { Component } from 'react'
import {
    IconCari,
    FilterdanText,
    LocationsH,
    TingkatH,
    JenisH,
    Filter,
    Close,
    OrangHitam,
    IconTamKelompok,
} from '../../assets/icons';
import {
    back,
    test,

} from '../../assets/images';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import SwitchSelector from 'react-native-switch-selector';
import { Picker } from '@react-native-picker/picker';

export class PengajuanAnak extends Component {
    constructor(props) {
        super(props);
        this.state = {
            caridanatur: '',
            modalpilih: false,
            pilihdonatur: false,
            pilihdonaturNPB: false,
            admin: '',
            Anak: [],
            Donatur: [],
        }
    }
    GetAnakAPi() {
        fetch('https://kilauindonesia.org/datakilau/api/joindataanak')
            .then(res => {
                if (res.status === 200) return res.json();
            })
            .then(resdata => {
                console.log(resdata.data);
                this.setState({
                    Anak: resdata.data,
                    filter: resdata.DATA,
                    refreshing: false,
                });
            });
    }
    GetDonaturAPi() {
        fetch('https://kilauindonesia.org/datakilau/api/joindataanak')
            .then(res => {
                if (res.status === 200) return res.json();
            })
            .then(resdata => {
                console.log(resdata.data);
                this.setState({
                    Donatur: resdata.data,
                    filter: resdata.DATA,
                    refreshing: false,
                });
            });
    }
    componentDidMount() {
        this.GetAnakAPi();
        this.GetDonaturAPi();
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        // Firebase.initializeApp(this);
        // this.requestCameraPermission();
        console.log(this.props);
    }

    render() {
        const admin = [
            { label: 'List CPB', value: 'CPB' },
            { label: 'List NPB', value: 'NPB' },
        ];
        const filter_NPB = this.state.Anak.filter(item => item.status_cpb === 'NPB')
        const filter_sta = this.state.Anak.filter(item => item.hasil_survey === 'Layak' && item.status_cpb === 'PB' || item.status_cpb === 'CPB' )
        return (
            <View>
                <SafeAreaView style={{ backgroundColor: '#fff' }}>

                    <View style={{ backgroundColor: '#0EBEDF', height: 164 }}>
                        <View
                            style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                            <TextInput
                                style={{
                                    flexDirection: 'row',
                                    backgroundColor: '#FFF',
                                    paddingHorizontal: 40,
                                    height: 38,
                                    width: 250,
                                    borderRadius: 9,
                                    marginLeft: 10,
                                }}
                                color={'#000'}
                                onChangeText={text => this.filterList(text)}
                                placeholder="Cari"
                                placeholderTextColor="#C0C0C0"
                                underlineColorAndroid="transparent"
                            />
                            <IconCari style={style.IconCari} name="your-icon" size={20} />
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ fil: true });
                                }}>
                                <FilterdanText style={{ marginLeft: 20 }} />
                            </TouchableOpacity>


                        </View>

                        <View style={{ marginTop: 30, marginHorizontal: 20 }}>
                            <SwitchSelector
                                fontSize={12}
                                fontFamily={'Poppins-Medium'}
                                options={admin}
                                initial={0}
                                borderWidth={0}
                                height={49}
                                borderRadius={10}
                                hasPadding
                                // onPress={(value) => console.log(`Call onPress with value: ${value}`)}
                                onPress={value => {
                                    this.setState({ admin: value }),
                                        ToastAndroid.show(value.toString(), ToastAndroid.SHORT);
                                }}

                            />
                        </View>

                    </View>


                    {/* <View style={{flexDirection:'row'}}> */}
                    {this.state.admin === 'CPB' | this.state.admin === '' ?
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={() => this.onRefresh()}
                                />
                            }
                            data={filter_sta}
                            renderItem={({ item }) => (
                                <View>
                                    <View style={{}}>
                                        <TouchableOpacity
                                            onPress={() => this.setState({ modalpilih: true })}>

                                            <View style={style.itemflat}>
                                                <View style={{
                                                    width: '10%', justifyContent: 'center',
                                                    backgroundColor: item.status_cpb === 'CPB' ? '#0076B8' : '#000' &&
                                                        item.status_cpb === 'PB' ? '#00B855' : '#000' && item.status_cpb === 'NPB' ? '#E32845' : '#000' && item.status_cpb === 'BCPB' ? '#FFBB0C' : '#000'
                                                }}>
                                                    <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', alignContent: 'center' }}>
                                                        <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', color: '#fff', transform: [{ rotate: '-90deg' }] }}>{item.status_cpb}</Text>
                                                    </View>
                                                </View>
                                                <View
                                                    style={{
                                                        height: 90,
                                                        width: '100%',
                                                        justifyContent: 'center',
                                                    }}>
                                                    <View
                                                        style={{
                                                            flexDirection: 'row',
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                        }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image source={test}
                                                                style={{
                                                                    height: 50,
                                                                    width: 50,
                                                                    borderRadius: 45,
                                                                    color: '#000',
                                                                    marginRight: 30
                                                                }}
                                                            />
                                                            <View
                                                                style={{ flexDirection: 'column', marginLeft: '-10%', justifyContent: 'center', width: '70%' }}>
                                                                <Text style={{ color: '#000', fontFamily: 'Poppins-Medium', fontSize: 14, marginLeft: 10, }}>
                                                                    {item.full_name}
                                                                </Text>
                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                        <JenisH style={{ marginLeft: 10, width: 25, height: 25 }} />
                                                                        {
                                                                            item.hasil_survey === '' | item.hasil_survey === 'null' | item.hasil_survey === null ?
                                                                                <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5, marginTop: 5 }}>Tidak Layak</Text>
                                                                                :
                                                                                <Text style={style.labelkiri}>{item.hasil_survey}</Text>
                                                                        }
                                                                    </View>

                                                                    {/* <View style={{ flexDirection: 'row', }}>
                                                                        <ProfilHitam style={{ marginLeft: 10, }} />
                                                                        <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5 }}>Donatur</Text>
                                                                    </View> */}

                                                                    <View style={{ flexDirection: 'row', }}>
                                                                        <OrangHitam style={{ marginLeft: 10, }} />
                                                                        {
                                                                            item.nama_lengkap === '' | item.nama_lengkap === 'null' | item.nama_lengkap === null ?
                                                                                <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5, marginTop: 5 }}>Belum Memiliki Donatur</Text>
                                                                                :
                                                                                <Text style={style.labelkanan}>{item.nama_lengkap}</Text>
                                                                        }
                                                                    </View>

                                                                </View>

                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            )}></FlatList>
                        :
                        <View></View>
                    }
                    {this.state.admin === 'NPB' ?
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={() => this.onRefresh()}
                                />
                            }
                            data={filter_NPB}
                            renderItem={({ item }) => (
                                <View>
                                    <View style={{}}>
                                        <TouchableOpacity
                                            onPress={() => this.setState({ modalpilih: true })}>

                                            <View style={style.itemflat}>
                                                <View style={{
                                                    width: '10%', justifyContent: 'center',
                                                    backgroundColor: item.status_cpb === 'CPB' ? '#0076B8' : '#000' &&
                                                        item.status_cpb === 'PB' ? '#00B855' : '#000' && item.status_cpb === 'NPB' ? '#E32845' : '#000' && item.status_cpb === 'BCPB' ? '#FFBB0C' : '#000'
                                                }}>
                                                    <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', alignContent: 'center' }}>
                                                        <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', color: '#fff', transform: [{ rotate: '-90deg' }] }}>{item.status_cpb}</Text>
                                                    </View>
                                                </View>
                                                <View
                                                    style={{
                                                        height: 90,
                                                        width: '100%',
                                                        justifyContent: 'center',
                                                    }}>
                                                    <View
                                                        style={{
                                                            flexDirection: 'row',
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                        }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image source={test}
                                                                style={{
                                                                    height: 50,
                                                                    width: 50,
                                                                    borderRadius: 45,
                                                                    color: '#000',
                                                                    marginRight: 30
                                                                }}
                                                            />
                                                            <View
                                                                style={{ flexDirection: 'column', marginLeft: '-10%', justifyContent: 'center', width: '70%' }}>
                                                                <Text style={{ color: '#000', fontFamily: 'Poppins-Medium', fontSize: 14, marginLeft: 10, }}>
                                                                    {item.full_name}
                                                                </Text>
                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                        <JenisH style={{ marginLeft: 10, width: 25, height: 25 }} />
                                                                        {
                                                                            item.hasil_survey === '' | item.hasil_survey === 'null' | item.hasil_survey === null ?
                                                                                <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5, marginTop: 5 }}>Tidak Layak</Text>
                                                                                :
                                                                                <Text style={style.labelkanan}>{item.hasil_survey}</Text>
                                                                        }
                                                                    </View>

                                                                    {/* <View style={{ flexDirection: 'row', }}>
                                                                    <ProfilHitam style={{ marginLeft: 10, }} />
                                                                    <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5 }}>Donatur</Text>
                                                                </View> */}

                                                                    <View style={{ flexDirection: 'row', }}>
                                                                        <OrangHitam style={{ marginLeft: 10, }} />
                                                                        {
                                                                            item.nama_lengkap === '' | item.nama_lengkap === 'null' | item.nama_lengkap === null ?
                                                                                <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5, marginTop: 5 }}>Belum Memiliki Donatur</Text>
                                                                                :
                                                                                <Text style={style.labelkanan}>{item.nama_lengkap}</Text>
                                                                        }
                                                                    </View>

                                                                </View>

                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            )}></FlatList>
                        :
                        <View></View>

                    }
                </SafeAreaView >

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    propagateSwipe={true}
                    visible={this.state.pilihdonaturNPB}
                    onRequestClose={() => this.setState({ pilihdonaturNPB: false })}
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    <View style={style.ModalCont2}>
                        <View style={{
                            paddingTop: 5,
                            backgroundColor: '#ffffff',
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10,
                            height: '100%',
                            shadowColor: "#333",
                            shadowOffset: {
                                width: 1,
                                height: 1,
                            },
                            shadowOpacity: 0.3,
                            shadowRadius: 2,
                            elevation: 3,
                            alignItems: 'center',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            right: 0,
                        }}>
                            <SafeAreaView style={{ width: '100%', height: '100%' }}>

                            </SafeAreaView>
                        </View>
                    </View>
                </Modal>

                <Modal animationType={"fade"} transparent={true}
                    visible={this.state.pilihdonatur}
                    onRequestClose={() => { this.setState({ pilihdonatur: false }) }}>

                    <SafeAreaView style={style.containerSafe}>
                        <TouchableOpacity activeOpacity={1.0} onPress={() => this.setState({ pilihdonatur: false })} style={style.ModalCont}>
                            <View style={{
                                paddingTop: 5,
                                marginHorizontal: 10,
                                backgroundColor: '#ffffff',
                                // flexDirection: 'row',
                                borderRadius: 20,
                                height: 430,
                                width: '100%',
                                shadowColor: "#333",
                                shadowOffset: {
                                    width: 1,
                                    height: 1,
                                },
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                                elevation: 3,
                            }}>
                                <View style={{ backgroundColor: '#0EBEDF', height: 100, marginTop: -10, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                                    <TouchableOpacity onPress={() => this.setState({ pilihdonatur: false })} style={{ position: 'absolute', right: 20, top: 20 }}>
                                        <Close />
                                    </TouchableOpacity>
                                    <Text style={style.txtPresensi}>Pilih Donatur</Text>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        backgroundColor: '#fff',
                                        borderWidth: 0.5,
                                        height: 40,
                                        borderRadius: 5,
                                        margin: 10,
                                        borderColor: '#C0C0C0'
                                    }}>
                                        <TextInput
                                            value={this.state.caridanatur}
                                            placeholder={'Cari Donatur'}
                                            onChangeText={(text) => {
                                                this.filterList(text.toLowerCase()), this.setState({ caridanatur: text })
                                            }}
                                            style={style.searchBar} />
                                    </View>

                                </View>
                                <ScrollView style={{ marginTop: 10, marginBottom: 30 }}>

                                    <View>
                                        <FlatList
                                            refreshControl={
                                                <RefreshControl
                                                    refreshing={this.state.refreshing}
                                                    onRefresh={() => this.onRefresh()}
                                                />
                                            }
                                            data={this.state.Donatur}
                                            renderItem={({ item }) => (
                                                <View>
                                                    <View style={{}}>
                                                        <TouchableOpacity
                                                            onPress={() => this.setState({ pilihdonatur: true })}>

                                                            <View style={style.itemflat2}>
                                                                <Text style={{ marginLeft: 10 }}>{item.nick_name}</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>

                                            )}></FlatList>
                                    </View>
                                </ScrollView>


                            </View>
                        </TouchableOpacity>
                    </SafeAreaView>
                </Modal>


                <Modal animationType={"fade"} transparent={true}
                    visible={this.state.modalpilih}
                    onRequestClose={() => this.setState({ modalpilih: false })}>

                    <View style={{
                        backgroundColor: '#fff',
                        paddingTop: 5,
                        marginHorizontal: 5,
                        marginTop: '50%',
                        borderRadius: 20,
                        height: '35%',
                        width: '90%',
                        borderWidth: 1,
                        borderColor: '#bdbdbd',
                        alignItems: 'center',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignSelf: 'center'
                    }}>
                        <SafeAreaView style={{ alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                <View style={style.kotakkecil}>
                                    <TouchableOpacity onPress={() => this.setState({ pilihdonatur: true, modalpilih: false })}>
                                        <IconTamKelompok style={{ justifyContent: 'center', alignSelf: 'center' }} />
                                        <Text style={{ marginTop: 10, textAlign: 'center', padding: 5 }}>Pilih Donatur</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={style.kotakkecil}>
                                    <TouchableOpacity onPress={() => this.setState({ modalpilih: false })}>
                                        {/* <KKada style={{ justifyContent: 'center', alignSelf: 'center' }} /> */}
                                        <Text style={{ marginTop: 10, textAlign: 'center', padding: 5 }}>Hapus Donatur</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ marginTop: '2%', alignItems: 'center', flexDirection: 'row', alignContent: 'center', }}>
                                <TouchableOpacity onPress={() => this.setState({ modalpilih: false }) + ToastAndroid.show('Batal', ToastAndroid.SHORT)}>
                                    <View style={{ height: 50, borderColor: '#00A9B8', borderWidth: 1, width: 100, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 5 }}>
                                        <Text style={{ color: '#00A9B8', fontFamily: 'Poppins-Medium', fontSize: 14 }}>Batal</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </SafeAreaView>
                    </View >

                </Modal >

            </View>
        )
    }
}
const style = StyleSheet.create({
    itemflat: {
        flex: 1,
        fontSize: 12,
        flexDirection: 'row',
        marginLeft: 10,
        paddingRight: 30,
        backgroundColor: '#fff',
        color: '#000',
        marginVertical: 10,
        marginHorizontal: 16,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderColor: '#7e7e7e',
        borderRadius: 15,
    },
    itemflat2: {
        flex: 1,
        fontSize: 12,
        flexDirection: 'row',
        marginLeft: 10,
        paddingRight: 30,
        backgroundColor: '#fff',
        color: '#000',
        marginVertical: 10,
        marginHorizontal: 16,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderColor: '#7e7e7e',
        borderRadius: 5,
        height: 30
    },
    IconCari: {
        position: 'absolute',
        top: 8,
        left: 20,
    },
    ModalCont2: {
        flex: 1,
    },
    containerSafe: {
        flex: 1,
        flexDirection: 'column',
    },
    txtPresensi: {
        justifyContent: 'center', alignItems: 'center',
        fontSize: 18,
        marginTop: 20,
        fontWeight: 'bold',
        color: '#fff',
        textAlign: 'center',

    },
    ModalCont: {
        flex: 1,
        justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#00000099',
        paddingHorizontal: 10,
    },
    kotakkecil: {
        flexDirection: 'column',
        borderColor: '#bdbdbd',
        borderWidth: 1,
        width: '40%',
        height: 160,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        textAlign: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 10,
        marginHorizontal: 10,
    },
    labelkanan: {
        color: '#000', fontSize: 10,
        marginHorizontal: 5,
        width: 100,
    },
    labelkiri: {
        color: '#000', fontSize: 10,marginTop:0,marginLeft:5,
        marginVertical: 5,
        width: 50,
    },
});
export default PengajuanAnak