import { Text, View, TouchableOpacity, Image, StyleSheet, ScrollView } from 'react-native'
import React, { Component } from 'react'
import {
  search,
  arrow,
  plus,
  Background,
  addfoto,
  Union,
  x,
  test,
  orang1
} from '../../assets/images';
import {
  Plus,
  Math,
  ArrowleftWhite,
} from '../../assets/icons';
export class DetailAktifitas extends Component {
  render() {
    return (
      <ScrollView style={{ backgroundColor: '#fff', height: '100%' }}>
        <View style={{ backgroundColor: '#0EBEDF', height: 164, borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
          <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'center', alignSelf: 'center', alignContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#fff', fontSize: 20, textAlign: 'center', fontWeight: 'bold', marginTop: -5, }}>Detail Aktifitas</Text>
          </View>
        </View>
        <View style={style.itemflat}>
          <Image source={addfoto} style={style.img2}></Image>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>

            <View style={{ flexDirection: 'row', marginTop: 10,marginLeft:10 }}>
              <Image
                source={orang1}
                style={{
                  justifyContent: 'center',
                  height: 40,
                  width: 40,
                  borderRadius: 45,
                  color: '#000',
                }}
              />
              {/* <View style={style.Label1}>
                        <Text>{item.nama} </Text>
                        <Text>{item.email}</Text>
                      </View> */}
              <View
                style={{ flexDirection: 'column', marginLeft: 10 }}>
                <Text
                  style={{
                    color: '#000',
                    fontFamily: 'Poppins-Medium',
                    fontSize: 14,
                  }}>
                  Vildan Vinanda
                </Text>
                <Math />
              </View>
            </View>
            
          </View>
          <View
            style={{
              borderWidth: 0.3,
              borderColor: '#EBEAEA',
              width: '100%',
              marginTop: 10,
            }}
          />

          <Text
            style={{
              color: '#5D5C5D',
              fontFamily: 'Poppins-SemiBold',
              fontSize: 16,
              marginTop: 10,
              marginLeft:10,
            }}>
            Belajar Perkalian Dasar
          </Text>
          <Text
            style={{
              color: '#5D5C5D',
              fontFamily: 'Poppins-Regular',  
              fontSize: 14,
              marginTop: 10,
              marginHorizontal:10,
              textAlign:'justify'
            }}>
           Anak-anak belajar untuk menghafal perkalian 1 sampai dengan 5. Ada beberapa anak yang dapat menghafal dengan cepat ada juga anak yang membutuhkan treatment tertentu dalam menghafal.
          </Text>
          <View
            style={{
            top:'12%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal:20
            }}>
            <Text style={style.labelbaru5}>23 Jan 2022</Text>
            <Text style={style.labelbaru5}> 10.45</Text>
          </View>
        </View>

      </ScrollView>
    )
  }
}
const style = StyleSheet.create({
  img2: {
    width: '90%',
    height: 270,
    marginTop: 10,
    borderRadius: 10,
    alignSelf: 'center',
  },
  itemflat: {
    flex: 1,
    fontSize: 12,
    backgroundColor: '#fff',
    color: '#000',
    marginVertical: 10,
    alignSelf: 'center',
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderColor: '#7e7e7e',
    borderRadius: 15,
    height:550,
    width: '90%',
    marginTop: -90,
  },

});

export default DetailAktifitas