import { Text, View, StyleSheet, ScrollView, Dimensions, TextInput, TouchableOpacity,ToastAndroid } from 'react-native'
import React, { Component } from 'react'
import { Picker } from '@react-native-picker/picker';
import { Date, } from '../../assets/icons';
import DateTimePicker from 'react-native-modal-datetime-picker';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
export class editdataibu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            detail: this.props.route.params.detail,
            nik_ibu: this.props.route.params.detail.nik_ibu,
            nama_ibu: this.props.route.params.detail.nama_ibu,
            agama: this.props.route.params.detail.agama,
            tempat_lahir: this.props.route.params.detail.tempat_lahir,
            alamat: this.props.route.params.detail.alamat,
            date1: new Date(),
            date2: new Date(),
            chosenDate1: this.props.route.params.detail.tanggal_lahir,
            chosenDate2: this.props.route.params.detail.tanggal_kematian,
            prov: [],
            id_prov: this.props.route.params.detail.id_prov,
            datakota: [],
            id_kab: this.props.route.params.detail.id_kab,
            datakec: [],
            id_kec: this.props.route.params.detail.id_kec,
            datakel: [],
            id_kel: this.props.route.params.detail.id_kel,
            penghasilan: this.props.route.params.detail.penghasilan,
            penyebab_kematian: this.props.route.params.detail.penyebab_kematian,
            provinsi: this.props.route.params.detail.provinsi,
            kabupaten: this.props.route.params.detail.kabupaten,
            kecamatan: this.props.route.params.detail.kecamatan,
            kelurahan: this.props.route.params.detail.kelurahan,
            ortu: this.props.route.params.detail1.status_ortu,
            isVisible1: false,
            isVisible2: false,
        };
    }
    GetprovAPi() {
        fetch('https://kilauindonesia.org/datakilau/api/getprovinsi').then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                prov: resdata.data

            })
        })
    }
    GetkabuAPi() {
        fetch('https://kilauindonesia.org/datakilau/api/getkab/' + this.state.id_prov).then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                datakota: resdata.data

            })
        })
    }
    GetkecAPi() {
        fetch('https://kilauindonesia.org/datakilau/api/getkec/' + this.state.id_kab).then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                datakec: resdata.data

            })
        })
    }
    GetkelAPi() {
        fetch('https://kilauindonesia.org/datakilau/api/getkel/' + this.state.id_kec).then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                datakel: resdata.data

            })
        })
    }

    editData() {
        {
            let simpandata = {
                nik_ibu: this.state.nik_ibu,
                nama_ibu: this.state.nama_ibu,
                agama: this.state.agama,
                tempat_lahir: this.state.tempat_lahir,
                tanggal_lahir: this.state.chosenDate1,
                alamat: this.state.alamat,
                id_prov: this.state.id_prov,
                id_kab: this.state.id_kab,
                id_kec: this.state.id_kec,
                id_kel: this.state.id_kel,
                penghasilan: this.state.penghasilan,
                tanggal_kematian: this.state.chosenDate2,
                penyebab_kematian: this.state.penyebab_kematian,

            }
            let data = new FormData();
            for (let key in simpandata) {
                data.append(key, simpandata[key]);
            }
            fetch('https://kilauindonesia.org/datakilau/api/ibuupd/' + this.state.detail.id_keluarga, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: data,
            })
                .then((res) => res.json())
                .then((resJson) => {
                    console.log(resJson);
                    if (resJson.status === 'sukses') {

                        ToastAndroid.show("Data berhasil ditambah!", ToastAndroid.SHORT)
                    } else {
                        alert(`Data gagal disimpan !!!`);
                    }
                })
                .catch((err) => console.log('dari catch send Data ===', err));
        }

    }
    componentDidMount() {
        this.GetprovAPi();
        this.GetkabuAPi();
        this.GetkecAPi();
        this.GetkelAPi();
        console.log(this.state.ortu);
    }
    showPicker1 = () => {
        this.setState({ isVisible1: true })
    }
    showPicker2 = () => {
        this.setState({ isVisible2: true })
    }
    handlePicker1 = (date) => {
        this.setState({ isVisible1: false, chosenDate1: moment(date).format('DD-MM-YYYY') })
    }
    handlePicker2 = (date) => {
        this.setState({ isVisible2: false, chosenDate2: moment(date).format('DD-MM-YYYY') })
    }
    render() {
        const detail = this.state.detail

        return (
            <ScrollView style={{ backgroundColor: '#fff', flex: 1 }}>
                <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', marginTop: 10 }}>Edit Data Ibu</Text>

                <Text style={style.labelatas}>Data Keluarga {this.state.ortu}</Text>
                <View style={{ justifyContent: 'center' }}>
                    <View style={style.form}>
                        <Text style={style.labelkiri}>NIK Ibu</Text>
                        <TextInput
                            style={style.kotak3}
                            onChangeText={nik_ibu => this.setState({ nik_ibu })}
                            value={this.state.nik_ibu}
                            keyboardType='numeric'
                            placeholder=""
                            placeholderTextColor="#C0C0C0"
                        />
                    </View>
                    <View style={style.form}>
                        <Text style={style.labelkiri}>Nama Ibu</Text>
                        <TextInput
                            style={style.kotak3}
                            onChangeText={nama_ibu => this.setState({ nama_ibu })}
                            value={this.state.nama_ibu}
                            keyboardType='default'
                            placeholder=""
                            placeholderTextColor="#C0C0C0"
                        />
                    </View>

                    <View style={style.form}>
                        <Text style={style.labelkiri}>Agama</Text>
                        <View style={style.kotakpicker}>
                            <Picker
                                style={style.Textinputcss}
                                selectedValue={this.state.agama}
                                onValueChange={itemValue =>
                                    this.setState({ agama: itemValue, show: 1 })
                                }>
                                <Picker.Item
                                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                    label={this.state.agama}
                                    value={this.state.agama}
                                />
                                <Picker.Item
                                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                    label="Islam"
                                    value="Islam"
                                />
                                <Picker.Item
                                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                    label="Kristen"
                                    value="Kristen"
                                />
                                <Picker.Item
                                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                    label="Hindu"
                                    value="Hindu"
                                />
                                <Picker.Item
                                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                    label="Budha"
                                    value="Budha"
                                />
                            </Picker>
                        </View>
                    </View>
                    <View style={style.form}>
                        <Text style={style.labelkiri}>Tempat Lahir</Text>
                        <TextInput
                            style={style.kotak3}
                            onChangeText={tempat_lahir => this.setState({ tempat_lahir })}
                            value={this.state.tempat_lahir}
                            keyboardType='default'
                            placeholder=""
                            placeholderTextColor="#C0C0C0"
                        />
                    </View>

                    <View style={style.form}>
                        <Text style={style.labelkiri}>Tanggal Lahir</Text>
                        <TextInput
                            style={style.kotak3}
                            onChangeText={chosenDate1 => this.setState({ chosenDate1 })}
                            value={this.state.chosenDate1}
                            keyboardType='default'
                            placeholder=""
                            placeholderTextColor="#C0C0C0"
                        />
                        <View
                            style={{
                                borderColor: '#DDD',
                                borderWidth: 1,
                                height: 50,
                                width: 50,
                                borderRadius: 10,
                                top: 10,
                                marginLeft: 10,

                            }}>
                            <TouchableOpacity TouchableOpacity onPress={this.showPicker1}>
                                <View style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 10, }}>
                                    <Date />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isVisible1}
                            onConfirm={this.handlePicker1}
                            onCancel={this.hidePicker}
                            mode={'date'}
                            is24Hour={true}
                        />
                    </View>

                    <View style={style.form}>
                        <Text style={style.labelkiri}>Alamat</Text>
                        <TextInput
                            style={style.kotakalamat}
                            onChangeText={alamat => this.setState({ alamat })}
                            value={this.state.alamat}
                            multiline={true}
                            keyboardType='default'
                            placeholder=""
                            placeholderTextColor="#C0C0C0"
                        />
                    </View>


                    <View style={style.form}>
                        <Text style={style.labelkiri}>Provinsi</Text>
                        <View
                            style={style.kotakpicker}>
                            <Picker
                                style={style.Textinputcss}
                                selectedValue={this.state.id_prov}
                                onValueChange={(itemValue,) => {
                                    this.setState({
                                        id_prov: itemValue
                                    })
                                }}>
                                {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                                <Picker.Item style={{ fontSize: 12 }} label={this.state.provinsi} value={'0'} key={'0'} />
                                {
                                    this.state.prov.map((provibu) =>
                                        <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={provibu.nama.toString()} value={provibu.id_prov} key={provibu.id_prov} />
                                    )}
                            </Picker>
                        </View>
                    </View>

                    <View style={style.form}>
                        <Text style={style.labelkiri}>Kota/Kabupaten</Text>
                        <View
                            style={style.kotakpicker}>
                            <Picker
                                style={style.Textinputcss}
                                selectedValue={this.state.id_kab}
                                onFocus={() => { this.GetkabuAPi() }}
                                onValueChange={(itemValue, prov) => {
                                    this.setState({
                                        id_kab: itemValue
                                    })
                                }}>
                                {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                                <Picker.Item style={{ fontSize: 12 }} label={detail.kabupaten} value={'0'} key={'0'} />
                                {
                                    this.state.datakota.map((kota) =>
                                        <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kota.nama.toString()} value={kota.id_kab} key={kota.id_kab} />
                                    )}
                            </Picker>
                        </View>
                    </View>

                    <View style={style.form}>
                        <Text style={style.labelkiri}>Kecamatan</Text>
                        <View
                            style={style.kotakpicker}>
                            <Picker
                                style={style.Textinputcss}
                                selectedValue={this.state.id_kec}
                                onFocus={() => { this.GetkecAPi() }}
                                onValueChange={(itemValue, datakota) => {
                                    this.setState({
                                        id_kec: itemValue
                                    })
                                }}>
                                {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                                <Picker.Item style={{ fontSize: 12 }} label={detail.kecamatan} value={'0'} key={'0'} />
                                {
                                    this.state.datakec.map((kec) =>
                                        <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kec.nama.toString()} value={kec.id_kec} key={kec.id_kec} />
                                    )}
                            </Picker>
                        </View>
                    </View>

                    <View style={style.form}>
                        <Text style={style.labelkiri}>Kelurahan</Text>
                        <View
                            style={style.kotakpicker}>
                            <Picker
                                style={style.Textinputcss}
                                selectedValue={this.state.id_kel}
                                onFocus={() => { this.GetkelAPi() }}
                                onValueChange={(itemValue, datakec) => {
                                    this.setState({
                                        id_kel: itemValue
                                    })
                                }}>
                                {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                                <Picker.Item style={{ fontSize: 12 }} label={this.state.detail.kelurahan} value={'0'} key={'0'} />
                                {
                                    this.state.datakel.map((kel) =>
                                        <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kel.nama.toString()} value={kel.id_kel} key={kel.id_kel} />
                                    )}
                            </Picker>
                        </View>
                    </View>

                    <View style={style.form}>
                        <Text style={style.labelkiri}>Penghasilan {this.props.route.params.detail.status_ortu}</Text>
                        <View
                            style={style.kotakpicker}>
                            <Picker style={style.Textinputcss}
                                selectedValue={this.state.penghasilan}
                                onValueChange={(itemValue,) => {
                                    this.setState({
                                        penghasilan: itemValue
                                    })
                                }}>
                                <Picker.Item style={{ fontSize: 12 }} label={this.state.penghasilan} value={'0'} key={'0'} />
                                <Picker.Item label="Dibawah Rp.500.000,-" value="Dibawah Rp.500.000,-" />
                                <Picker.Item label="Rp.500.000,- s/d Rp.1.500.000,-" value="Rp.500.000,- s/d Rp.1.500.000,-" />
                                <Picker.Item label="Rp.1.500.000,- s/d Rp.2.500.000,-" value="Rp.1.500.000,- s/d Rp.2.500.000,-" />
                                <Picker.Item label="Rp.2.500.000,- s/d Rp.3.500.000,-" value="Rp.2.500.000,- s/d Rp.3.500.000,-" />
                                <Picker.Item label="Rp.3.500.000,- s/d Rp.5.000.000,-" value="Rp.3.500.000,- s/d Rp.5.000.000,-" />
                                <Picker.Item label="Rp.5.000.000,- s/d Rp.7.000.000,-" value="Rp.5.000.000,- s/d Rp.7.000.000,-" />
                                <Picker.Item label="Rp.7.000.000,- s/d Rp.10.000.000,-" value="Rp.7.000.000,- s/d Rp.10.000.000,-" />
                                <Picker.Item label="Diatas Rp.10.000.000,-" value="Diatas Rp.10.000.000,-" />
                            </Picker>
                        </View>
                    </View>
                    {this.state.stat === 'yatim_piatu' | this.state.stat === 'piatu' ?
                        <View>
                            <Text>*isi Jika Meninggal</Text>

                            <View style={style.form}>
                                <Text style={style.labelkiri}>Tanggal Kematian</Text>
                                <TextInput
                                    style={style.kotak3}
                                    onChangeText={chosenDate2 => this.setState({ chosenDate2 })}
                                    value={this.state.chosenDate2}
                                    keyboardType='default'
                                    placeholder=""
                                    placeholderTextColor="#C0C0C0"
                                />
                                <View
                                    style={{
                                        borderColor: '#DDD',
                                        borderWidth: 1,
                                        height: 50,
                                        width: 50,
                                        borderRadius: 10,
                                        top: 10,
                                        marginLeft: 10,

                                    }}>
                                    <TouchableOpacity TouchableOpacity onPress={this.showPicker2}>
                                        <View style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 10, }}>
                                            <Date />
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <DateTimePicker
                                    isVisible={this.state.isVisible2}
                                    onConfirm={this.handlePicker2}
                                    onCancel={this.hidePicker}
                                    mode={'date'}
                                    is24Hour={true}
                                />
                            </View>
                            <View style={style.form}>
                                <Text style={style.labelkiri}>Penyebab Kematian</Text>
                                <TextInput
                                    style={style.kotak3}
                                    onChangeText={penyebab => this.setState({ penyebab })}
                                    value={this.state.penyebab}
                                    keyboardType='default'
                                    placeholder='Penyebab Kematian'
                                    placeholderTextColor="#C0C0C0"
                                />
                            </View>
                        </View>
                        :
                        <View />

                    }




                    <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', marginTop: 0, }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('detailkeluarga', this.editData())} style={style.refresh} >
                            <View style={{
                                top: 20,
                                backgroundColor: '#0EBEDF', width: 170, height: 40, justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 10
                            }}>
                                <Text style={{ color: '#fff', textAlign: 'center', justifyContent: 'center', alignContent: 'center' }}>Simpan</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </ScrollView >
        )
    }
}
const style = StyleSheet.create({
    form: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        width: '100%'
    },
    labelkiri: {
        fontSize: 12,
        fontWeight: 'bold',
        marginVertical: 5,
        marginLeft: 20,
        width: 100,
    },
    labelkiri1: {
        fontSize: 12,
        marginTop: 10,
        fontWeight: 'bold',
        marginVertical: 5,
        marginLeft: 30,
    },
    labelkiri2: {
        fontSize: 12,
        fontWeight: 'bold',
        marginVertical: 5,
        marginLeft: 20,
        width: 90,
    },
    labelkirianak: {
        fontSize: 12,
        fontWeight: 'bold',
        marginVertical: 5,
        marginLeft: 20,
        width: 200,
    },
    labelkanan: {
        fontSize: 12,
        marginHorizontal: 5,
        width: 150,
    },
    labelkk: {
        fontSize: 12,
        marginHorizontal: 5,
        width: 200,
    },
    labelkanan2: {
        fontSize: 12,
        marginHorizontal: 5,
        width: 220,
    },
    labelkanan3: {
        marginHorizontal: 5,
        width: 200,
        height: 40,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        justifyContent: 'center',
        alignContent: 'center'
    },
    labelkanan4: {
        marginHorizontal: 5,
        width: 200,
        height: 90,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#bdbdbd',
        justifyContent: 'center',
        alignContent: 'center'
    },
    kotak: {
        marginVertical: 10,
        marginHorizontal: 16,
        borderRadius: 15,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderColor: '#7e7e7e',
        height: 300
    },
    kotakalamat: {
        marginTop: 10,
        color: '#000',
        borderRadius: 10,
        borderWidth: 0.1,
        fontSize: 13,
        height: 100,
        width: windowWidth * 0.5,
        padding: 12,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        fontFamily: 'Poppins-Regular',
    },
    kotakbtn: {
        marginVertical: 10,
        marginHorizontal: 16,
        borderRadius: 15,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderColor: '#7e7e7e',
    },
    kotakank: {
        marginVertical: 10,
        marginHorizontal: 16,
        borderRadius: 15,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderColor: '#7e7e7e',
    },
    labelatas: {
        fontSize: 14,
        marginLeft: 20,
        marginBottom: 5,
        fontWeight: 'bold',
    },
    Textinputcss: {
        width: windowWidth - 200,
        color: '#C0C0C0',
        marginTop: -10,
        borderRadius: 10,
        borderWidth: 1,
        fontSize: 10,
        height: 40,
        borderColor: '#C0C0C0',
        fontFamily: 'Poppins-Regular',
    },
    refresh: {
        padding: 10,
        marginBottom: 10,
        borderRadius: 5,
        // marginHorizontal: 5,
        bottom: 10,
    },//vildan
    body: {
        backgroundColor: '#EEEEEE',
        borderColor: '#CECBCB',
        borderRadius: 10,
        borderWidth: 3,
        borderStyle: 'dashed',
        backgroundColor: '#ffff',
        marginTop: 10,
        width: '90%',
        height: 200,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 10
    },
    itemflat: {
        flex: 1,
        fontSize: 12,
        flexDirection: 'row',
        marginLeft: 15,
        paddingRight: 30,
        backgroundColor: '#fff',
        color: '#000',
        marginVertical: 10,
        marginHorizontal: 16,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderColor: '#7e7e7e',
        borderRadius: 15,
    },
    iconbesar: {
        marginTop: 50,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    kotak3: {
        marginTop: 10,
        color: '#000',
        borderRadius: 10,
        borderWidth: 0.1,
        fontSize: 13,
        height: 40,
        width: windowWidth * 0.5,
        padding: 12,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        fontFamily: 'Poppins-Regular',
    },
    kotakpicker: {
        marginTop: 10,
        paddingVertical: 5,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#DDD',
        width: windowWidth * 0.5,
        height: 40,
    },
    Label3: {
        marginTop: 10,
        fontSize: 16,
        width: '100%',
        color: '#000',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
    },
})
export default editdataibu