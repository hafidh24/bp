import { Text, View, TextInput, TouchableOpacity, ScrollView, StyleSheet, Dimensions } from 'react-native'
import React, { Component } from 'react'
import { Date, } from '../../assets/icons';
import { Picker } from '@react-native-picker/picker';
import DateTimePicker from 'react-native-modal-datetime-picker';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

export class editdatawali extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: this.props.route.params.detail,
      nikwali: '',
      namawali: '',
      agama: '',
      tempat: '',
      date: new Date(),
      chosenDate: '',
      chosenDate2: '',
      prov: [],
      provwali: '',
      datakota: [],
      kota: '',
      datakec: [],
      kec: '',
      datakel: [],
      kel: '',
      penghasilan: '',
      hubker: '',
    };
  }
  render() {
    const detail = this.state.detail

    return (
      <ScrollView style={{ backgroundColor: '#fff', flex: 1 }}>
        <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: 'bold', marginTop: 10 }}>Edit Data Wali</Text>
        <View>
          <Text style={style.labelatas}>Data Keluarga</Text>
          <View style={{ justifyContent: 'center' }}>
            <View style={style.form}>
              <Text style={style.labelkiri}>NIK wali</Text>
              <TextInput
                style={style.kotak3}
                onChangeText={nikwali => this.setState({ nikwali })}
                value={this.state.nikwali}
                keyboardType='numeric'
                placeholder={detail.nik_wali}
                placeholderTextColor="#C0C0C0"
              />
            </View>
            <View style={style.form}>
              <Text style={style.labelkiri}>Nama wali</Text>
              <TextInput
                style={style.kotak3}
                onChangeText={namawali => this.setState({ namawali })}
                value={this.state.namawali}
                keyboardType='default'
                placeholder={detail.nama_wali}
                placeholderTextColor="#C0C0C0"
              />
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Agama</Text>
              <View style={style.kotakpicker}>
                <Picker
                  style={style.Textinputcss}
                  selectedValue={this.state.agama}
                  onValueChange={itemValue =>
                    this.setState({ agama: itemValue, show: 1 })
                  }>
                  <Picker.Item
                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                    label={detail.agama}
                    value=""
                  />
                  <Picker.Item
                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                    label="Islam"
                    value="Islam"
                  />
                  <Picker.Item
                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                    label="Kristen"
                    value="Kristen"
                  />
                  <Picker.Item
                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                    label="Hindu"
                    value="Hindu"
                  />
                  <Picker.Item
                    style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                    label="Budha"
                    value="Budha"
                  />
                </Picker>
              </View>
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Tempat Lahir</Text>
              <TextInput
                style={style.kotak3}
                onChangeText={tempat => this.setState({ tempat })}
                value={this.state.tempat}
                keyboardType='default'
                placeholder={detail.tempat_lahir}
                placeholderTextColor="#C0C0C0"
              />
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Tanggal Lahir</Text>
              <TextInput
                style={style.kotak3}
                onChangeText={chosenDate => this.setState({ chosenDate })}
                value={this.state.chosenDate}
                keyboardType='default'
                placeholder={detail.tanggal_lahir}
                placeholderTextColor="#C0C0C0"
              />
              <View
                style={{
                  borderColor: '#DDD',
                  borderWidth: 1,
                  height: 50,
                  width: 50,
                  borderRadius: 10,
                  top: 10,
                  marginLeft: 10,

                }}>
                <TouchableOpacity TouchableOpacity onPress={this.showPicker}>
                  <View style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 10, }}>
                    <Date />
                  </View>
                </TouchableOpacity>
              </View>
              <DateTimePicker
                isVisible={this.state.isVisible}
                onConfirm={this.handlePicker}
                onCancel={this.hidePicker}
                mode={'date'}
                is24Hour={true}
              />
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Alamat</Text>
              <TextInput
                style={style.kotakalamat}
                onChangeText={alamat => this.setState({ alamat })}
                value={this.state.alamat}
                keyboardType='default'
                placeholder={detail.alamat}
                placeholderTextColor="#C0C0C0"
              />
            </View>


            <View style={style.form}>
              <Text style={style.labelkiri}>Provinsi</Text>
              <View
                style={style.kotakpicker}>
                <Picker
                  style={style.Textinputcss}
                  selectedValue={this.state.provwali}
                  onValueChange={(itemValue,) => {
                    this.setState({
                      provwali: itemValue
                    })
                  }}>
                  {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                  <Picker.Item style={{ fontSize: 12 }} label={'Pilih Provinsi'} value={'0'} key={'0'} />
                  {
                    this.state.prov.map((provwali) =>
                      <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={provwali.nama.toString()} value={provwali.id_prov} key={provwali.id_prov} />
                    )}
                </Picker>
              </View>
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Kota/Kabupaten</Text>
              <View
                style={style.kotakpicker}>
                <Picker
                  style={style.Textinputcss}
                  selectedValue={this.state.kota}
                  onFocus={() => { this.GetkabuAPi() }}
                  onValueChange={(itemValue, prov) => {
                    this.setState({
                      kota: itemValue
                    })
                  }}>
                  {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                  <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kabupaten/Kota'} value={'0'} key={'0'} />
                  {
                    this.state.datakota.map((kota) =>
                      <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kota.nama.toString()} value={kota.id_kab} key={kota.id_kab} />
                    )}
                </Picker>
              </View>
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Kecamatan</Text>
              <View
                style={style.kotakpicker}>
                <Picker
                  style={style.Textinputcss}
                  selectedValue={this.state.kec}
                  onFocus={() => { this.GetkecAPi() }}
                  onValueChange={(itemValue, datakota) => {
                    this.setState({
                      kec: itemValue
                    })
                  }}>
                  {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                  <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kecamatan'} value={'0'} key={'0'} />
                  {
                    this.state.datakec.map((kec) =>
                      <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kec.nama.toString()} value={kec.id_kec} key={kec.id_kec} />
                    )}
                </Picker>
              </View>
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Kelurahan</Text>
              <View
                style={style.kotakpicker}>
                <Picker
                  style={style.Textinputcss}
                  selectedValue={this.state.kel}
                  onFocus={() => { this.GetkelAPi() }}
                  onValueChange={(itemValue, datakec) => {
                    this.setState({
                      kel: itemValue
                    })
                  }}>
                  {/* <Picker.Item style={{ fontSize: 12 }} label={detail.id_prov === null ? 'Pilih Provinsi' : detail.nama_prov} value={'0'} key={'0'} /> */}
                  <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kecamatan'} value={'0'} key={'0'} />
                  {
                    this.state.datakel.map((kel) =>
                      <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kel.nama.toString()} value={kel.id_kel} key={kel.id_kel} />
                    )}
                </Picker>
              </View>
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Penghasilan</Text>
              <View
                style={style.kotakpicker}>
                <Picker style={style.Textinputcss}
                  selectedValue={this.state.penghasilan}
                  onValueChange={(itemValue,) => {
                    this.setState({
                      penghasilan: itemValue
                    })
                  }}>
                  <Picker.Item style={{ fontSize: 12 }} label={detail.penghasilan} value={'0'} key={'0'} />
                  <Picker.Item label="Dibawah Rp.500.000,-" value="0" />
                  <Picker.Item label="Rp.500.000,- s/d Rp.1.500.000,-" value="1" />
                  <Picker.Item label="Rp.1.500.000,- s/d Rp.2.500.000,-" value="2" />
                  <Picker.Item label="Rp.2.500.000,- s/d Rp.3.500.000,-" value="3" />
                  <Picker.Item label="Rp.3.500.000,- s/d Rp.5.000.000,-" value="4" />
                  <Picker.Item label="Rp.5.000.000,- s/d Rp.7.000.000,-" value="5" />
                  <Picker.Item label="Rp.7.000.000,- s/d Rp.10.000.000,-" value="6" />
                  <Picker.Item label="Diatas Rp.10.000.000,-" value="7" />
                </Picker>
              </View>
            </View>

            <View style={style.form}>
              <Text style={style.labelkiri}>Hubungan Kerabat</Text>
              <TextInput
                style={style.kotak3}
                onChangeText={hubker => this.setState({ hubker })}
                value={this.state.hubker}
                keyboardType='default'
                placeholder={detail.hub_kerabat}
                placeholderTextColor="#C0C0C0"
              />
            </View>
            <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', marginTop: 0, }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('')} style={style.refresh} >
                <View style={{
                  top: 20,
                  backgroundColor: '#0EBEDF', width: 170, height: 40, justifyContent: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 10
                }}>
                  <Text style={{ color: '#fff', textAlign: 'center', justifyContent: 'center', alignContent: 'center' }}>Simpan</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}
const style = StyleSheet.create({
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    width: '100%'
  },
  labelkiri: {
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 5,
    marginLeft: 20,
    width: 100,
  },
  labelkiri1: {
    fontSize: 12,
    marginTop: 10,
    fontWeight: 'bold',
    marginVertical: 5,
    marginLeft: 30,
  },
  labelkiri2: {
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 5,
    marginLeft: 20,
    width: 90,
  },
  labelkirianak: {
    fontSize: 12,
    fontWeight: 'bold',
    marginVertical: 5,
    marginLeft: 20,
    width: 200,
  },
  labelkanan: {
    fontSize: 12,
    marginHorizontal: 5,
    width: 150,
  },
  labelkk: {
    fontSize: 12,
    marginHorizontal: 5,
    width: 200,
  },
  labelkanan2: {
    fontSize: 12,
    marginHorizontal: 5,
    width: 220,
  },
  labelkanan3: {
    marginHorizontal: 5,
    width: 200,
    height: 40,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    justifyContent: 'center',
    alignContent: 'center'
  },
  labelkanan4: {
    marginHorizontal: 5,
    width: 200,
    height: 90,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    justifyContent: 'center',
    alignContent: 'center'
  },
  kotak: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 15,
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderColor: '#7e7e7e',
    height: 300
  },
  kotakbtn: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 15,
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderColor: '#7e7e7e',
  },
  kotakank: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 15,
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderColor: '#7e7e7e',
  },
  labelatas: {
    fontSize: 14,
    marginLeft: 20,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  Textinputcss: {
    width: windowWidth - 200,
    color: '#C0C0C0',
    marginTop: -10,
    borderRadius: 10,
    borderWidth: 1,
    fontSize: 10,
    height: 40,
    borderColor: '#C0C0C0',
    fontFamily: 'Poppins-Regular',
  },
  refresh: {
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
    // marginHorizontal: 5,
    bottom: 10,
  },//vildan
  body: {
    backgroundColor: '#EEEEEE',
    borderColor: '#CECBCB',
    borderRadius: 10,
    borderWidth: 3,
    borderStyle: 'dashed',
    backgroundColor: '#ffff',
    marginTop: 10,
    width: '90%',
    height: 200,
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 10
  },
  itemflat: {
    flex: 1,
    fontSize: 12,
    flexDirection: 'row',
    marginLeft: 15,
    paddingRight: 30,
    backgroundColor: '#fff',
    color: '#000',
    marginVertical: 10,
    marginHorizontal: 16,
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderColor: '#7e7e7e',
    borderRadius: 15,
  },
  iconbesar: {
    marginTop: 50,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  kotak3: {
    marginTop: 10,
    color: '#000',
    borderRadius: 10,
    borderWidth: 0.1,
    fontSize: 13,
    height: 40,
    width: windowWidth * 0.5,
    padding: 12,
    backgroundColor: '#fff',
    borderColor: '#DDDDDD',
    borderWidth: 1,
    fontFamily: 'Poppins-Regular',
  },
  kotakalamat: {
    marginTop: 10,
    color: '#000',
    borderRadius: 10,
    borderWidth: 0.1,
    fontSize: 13,
    height: 100,
    width: windowWidth * 0.5,
    padding: 12,
    backgroundColor: '#fff',
    borderColor: '#DDDDDD',
    borderWidth: 1,
    fontFamily: 'Poppins-Regular',
  },
  kotakpicker: {
    marginTop: 10,
    paddingVertical: 5,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#DDD',
    width: windowWidth * 0.5,
    height: 40,
  },
  Label3: {
    marginTop: 10,
    fontSize: 16,
    width: '100%',
    color: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Poppins-Medium',
  },
})
export default editdatawali