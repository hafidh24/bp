import { ScrollView, Text, View, StyleSheet, Dimensions, TextInput, TouchableOpacity, Image, Modal,ToastAndroid } from 'react-native'
import React, { Component } from 'react'
import { Picker } from '@react-native-picker/picker';
import DatePicker from 'react-native-date-picker';
import { x, date } from '../../assets/images'
export class Four extends Component {
    constructor(props) {
        super(props)
        this.state = {
            NIK: '',
            wali: '',
            agama: [],
            ag: '',
            lahir: '',
            tglahir: '',
            tgmati: '',
            alamat: '',
            prov: [],
            pro: '',
            kota: [],
            kot: '',
            kecamatan: [],
            kec: '',
            kelurahan: [],
            kel: '',
            penghasilan: [],
            gaji: '',
            deskripsi: '',
            totalSteps: "",
            currentStep: "",
            datewali: new Date(),
            modaldate: false,
            show: false,


        }
    }
    componentDidMount() {
        this.GetkotaAPi();
        this.GetprovAPi();
    }
    GetprovAPi() {
        fetch('https://berbagibahagia.org/api/getprov').then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                prov: resdata.data

            })
        })
    }
    GetkotaAPi() {
        fetch('https://berbagibahagia.org/api/getkota/' + this.state.pro).then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                kota: resdata.data

            })
        })
    }
    // static getDerivedStateFromProps = props => {
    //     const { getTotalSteps, getCurrentStep } = props;
    //     return {
    //         totalSteps: getTotalSteps(),
    //         currentStep: getCurrentStep()
    //     };
    // };

    // nextStep = () => {
    //     const { next, saveState } = this.props;
    //     saveState({ data: "" });
    //     next();
    // };
    render() {
        const { currentStep, totalSteps } = this.state;

        return (
            <ScrollView style={{ backgroundColor: '#fff' }}>
                <View style={{ backgroundColor: '#00A9B8' }}>
                    <Text style={style.title1}>Tambah Anak Binaan</Text>
                </View>
                <Text
                    style={style.currentStepText}
                >{`Tahap 6`}</Text>
                <View style={style.kotak5}>
                    <Text style={style.Label2}>NIK Wali</Text>
                    <TextInput
                        style={style.kotak3}
                        onChangeText={NIK => this.setState({ NIK })}
                        value={this.state.NIK}
                        keyboardType='numeric'
                        placeholder="NIK wali"
                        placeholderTextColor='#7e7e7e'
                    />
                </View>
                <View style={style.kotak5}>
                    <Text style={style.Label2}>Nama Wali</Text>
                    <TextInput
                        style={style.kotak3}
                        onChangeText={wali => this.setState({ wali })}
                        value={this.state.wali}
                        keyboardType='default'
                        placeholder="Nama wali"
                        placeholderTextColor='#7e7e7e'
                    />
                </View>
                <View>
                    <Text style={style.Label1}>Agama</Text>
                    <Picker style={style.Textinputcss} mode="dropdown"
                        selectedValue={this.state.ag}
                        onValueChange={(itemValue) => {
                            this.setState({
                                ag: itemValue
                            })
                        }}>
                        <Picker.Item style={{ fontSize: 12 }} label={'Pilih Agama'} value={'0'} key={'0'} />
                        <Picker.Item label="Islam" value="Islam" />
                        <Picker.Item label="Kristen" value="Kristen" />
                        <Picker.Item label="Budha" value="Budha" />
                        <Picker.Item label="Hindu" value="Hindu" />
                        <Picker.Item label="Konghuncu" value="Konghuncu" />
                    </Picker>
                </View>

                <View style={style.kotak5}>
                    <Text style={style.Label2}>Tempat Lahir</Text>
                    <TextInput
                        style={style.kotak3}
                        onChangeText={lahir => this.setState({ lahir })}
                        value={this.state.lahir}
                        keyboardType='default'
                        placeholder="Tempat lahir"
                        placeholderTextColor='#7e7e7e'
                    />
                </View>
                <View style={style.kotak5}>
                    <Text style={style.Label2}>Tanggal Lahir</Text>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-around', backgroundColor: '#F0F8FF', width: '90%', marginLeft: 20 }} onPress={() => this.setState({ modaldate: true })}>
                        <Image source={date}
                            style={{
                                height: 30,
                                width: 30,
                                marginTop: 5,
                                marginLeft: 20,
                            }}></Image>
                        <Text style={style.Label1}>{this.state.date.toLocaleString('default', { month: 'short' })}</Text>
                    </TouchableOpacity>
                </View>

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.modaldate}
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',

                    }}>
                    <View style={style.ModalCont2}>
                        <View style={{
                            paddingTop: 5,
                            backgroundColor: '#ffffff',
                            // flexDirection: 'row',
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10,
                            height: '27%',
                            shadowColor: "#333",
                            shadowOffset: {
                                width: 1,
                                height: 1,
                            },
                            shadowOpacity: 0.3,
                            shadowRadius: 2,
                            elevation: 3,
                            alignItems: 'center',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            right: 0,
                        }}>
                            <Text style={style.tglText}>Pilih Tanggal</Text>
                            <ScrollView style={{ width: '100%', height: '100%' }}>

                                <TouchableOpacity onPress={() => this.setState({ modaldate: false })} style={{ position: 'absolute', right: 20, top: 20 }}>
                                    <Image source={x}
                                        style={{
                                            height: 30,
                                            width: 30, alignItems: 'center',
                                        }}></Image>
                                </TouchableOpacity>
                                <DatePicker
                                    date={this.state.date}
                                    placeholder="select date"
                                    onDateChange={(date) =>
                                        this.setState({ date }, () => console.log(this.state.date))
                                    }
                                    androidVariant="nativeAndroid"
                                    mode='date'

                                />
                            </ScrollView>
                        </View>
                    </View>
                </Modal>

                <View>
                    <Text style={style.Label1}>Provinsi</Text>
                    <Picker style={style.Textinputcss} mode="dropdown"
                        selectedValue={this.state.pro}
                        onValueChange={(itemValue) => {
                            this.setState({
                                pro: itemValue
                            })
                        }}>
                        <Picker.Item style={{ fontSize: 12 }} label={'Pilih provinsi'} value={'0'} key={'0'} />
                        {
                            this.state.prov.map((pro) =>
                                <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={pro.name.toString()} value={pro.province_id} key={pro.province_id} />
                            )}
                    </Picker>
                </View>
                <View>
                    <Text style={style.Label1}>Kabupaten/Kota</Text>
                    <Picker style={style.Textinputcss} mode="dropdown" onFocus={() => { this.GetkotaAPi() }} //untuk get data yang terhubung dengan picker lain//
                        selectedValue={this.state.kot}
                        onValueChange={(itemValue, prov) => {
                            {
                                this.setState({ kot: (itemValue), })
                            }
                        }}>
                        <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kota'} value={'0'} key={'0'} />
                        {
                            this.state.kota.map((kot) =>
                                <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kot.name.toString()} value={kot.name.toString()} key={kot.name.toString()} />
                            )}
                    </Picker>
                </View>

                <View>
                    <Text style={style.Label1}>Kecamatan</Text>
                    <Picker style={style.Textinputcss} mode="dropdown"
                        selectedValue={this.state.kec}
                        onValueChange={(itemValue) => {
                            this.setState({
                                kec: itemValue
                            })
                        }}>
                        <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kecamatan'} value={'0'} key={'0'} />
                        {
                            this.state.kecamatan.map((kec) =>
                                <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={lv.id_level_anak_binaan.toString()} value={lv.id_level_anak_binaan.toString()} key={lv.id_level_anak_binaan.toString()} />
                            )}
                    </Picker>
                </View>
                <View>
                    <Text style={style.Label1}>Kelurahan</Text>
                    <Picker style={style.Textinputcss} mode="dropdown"
                        selectedValue={this.state.kel}
                        onValueChange={(itemValue) => {
                            this.setState({
                                kel: itemValue
                            })
                        }}>
                        <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kelurahan'} value={'0'} key={'0'} />
                        {
                            this.state.kelurahan.map((kel) =>
                                <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={lv.id_level_anak_binaan.toString()} value={lv.id_level_anak_binaan.toString()} key={lv.id_level_anak_binaan.toString()} />
                            )}
                    </Picker>
                </View>

                <View>
                    <Text style={style.Label1}>Penghasilan</Text>
                    <Picker style={style.Textinputcss} mode="dropdown"
                        selectedValue={this.state.gaji}
                        onValueChange={(itemValue) => {
                            this.setState({
                                gaji: itemValue
                            })
                        }}>
                        <Picker.Item style={{ fontSize: 12 }} label={'Pilih Penghasilan'} value={'0'} key={'0'} />
                        <Picker.Item label="Dibawah Rp.500.000,-" value="Kakak" />
                        <Picker.Item label="Rp.500.000,- s/d Rp.1.500.000,-" value="1" />
                        <Picker.Item label="Rp.1.500.000,- s/d Rp.2.500.000,-" value="2" />
                        <Picker.Item label="Rp.2.500.000,- s/d Rp.3.500.000,-" value="3" />
                        <Picker.Item label="Rp.3.500.000,- s/d Rp.5.000.000,-" value="4" />
                        <Picker.Item label="Rp.5.000.000,- s/d Rp.7.000.000,-" value="5" />
                        <Picker.Item label="Rp.7.000.000,- s/d Rp.10.000.000,-" value="6" />
                        <Picker.Item label="Diatas Rp.10.000.000,-" value="7" />

                    </Picker>
                </View>

                <View>
                    <Text style={style.Label1}>Hubungan Kerabat</Text>
                    <Picker style={style.Textinputcss} mode="dropdown"
                        selectedValue={this.state.hub}
                        onValueChange={(itemValue) => {
                            this.setState({
                                hub: itemValue
                            })
                        }}>
                        <Picker.Item style={{ fontSize: 12 }} label={'Pilih Hub.Kerabat'} value={'0'} key={'0'} />
                        <Picker.Item label="Kakak" value="Kakak" />
                        <Picker.Item label="Saudara dari Ayah" value="SA" />
                        <Picker.Item label="Saudara dari Ibu" value="SI" />
                        <Picker.Item label="Tidak Ada Hubungan Kerabat" value="TAHK" />
                    </Picker>
                </View>

                <TouchableOpacity style={style.btnSimpanDark} onPress={() =>
                    // this.state.NIK === '' ? alert('Tolong ada kolom yang belum terisi') :
                    //     this.state.ayah === '' ? alert('Tolong ada kolom yang belum terisi') :
                    //         this.state.ag === '' ? alert('Tolong ada kolom yang belum terisi') :
                    //             this.state.jarak === '' ? alert('Tolong ada kolom yang belum terisi') :
                    //                 this.state.lahir === '' ? alert('Tolong ada kolom yang belum terisi') :
                    //                     this.state.tglahir === '' ? alert('Tolong ada kolom yang belum terisi') :
                    //                         this.state.hub === '' ? alert('Tolong ada kolom yang belum terisi') :
                    //                             this.state.gaji === '' ? alert('Tolong ada kolom yang belum terisi') :
                    // this.sendData()
                    ToastAndroid.show("Data telah disimpan", ToastAndroid.SHORT)}>
                    <Text>Simpan</Text>
                </TouchableOpacity>
            </ScrollView>
        )
    }
}

export default Four
const style = StyleSheet.create({

    contentContainer: {
    },
    Label1: {
        flex: 1,
        fontSize: 15,
        marginTop: 10,
        marginBottom: -1,
        marginLeft: 30,
        color: '#000000',
        flexDirection: 'column',
    },
    Textinputcss: {
        color: '#7e7e7e',
        marginLeft: 15,
        marginRight: 10,
        borderRadius: 10,
        borderWidth: 1,
        fontSize: 12,
        height: 52,
        backgroundColor: '#fff',
        shadowColor: "#333",
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        borderColor: '#7e7e7e',
        elevation: 3,
    },
    Label2: {
        marginTop: 5,
        marginLeft: 25,
        padding: 5,
        color: '#000',
        alignItems: 'center',
        justifyContent: 'center'
    },
    kotak2: {
        color: '#000000',
        marginTop: 10,
        marginLeft: 30,
        marginRight: 10,
        borderRadius: 2,
        borderWidth: 0.1,
        fontSize: 12,
        height: 52,
        backgroundColor: '#7e7e7e',
    },
    title1: {
        marginRight: 20,
        marginLeft: 20,
        marginTop: 15,
        marginBottom: 15,
        fontSize: 18,
        fontWeight: 'bold',
        color: '#fff',
    },
    Labeltgl: {
        marginTop: 5,
        position: 'absolute',
        top: 0, left: 10, right: 0, bottom: 0,
        height: 25, width: 25,
    },
    item: {
        flex: 1,
        fontSize: 16,
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 17,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: "#333",
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        borderColor: '#7e7e7e',
        elevation: 3,
    },
    container: {
        marginTop: 40,
        marginLeft: 75,
        width: 250,
        height: 250,
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        shadowColor: "#333",
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        borderColor: '#7e7e7e',
        elevation: 3,
    },

    btnSimpanUn1: {
        width: '40%',
        fontWeight: 'bold',
        backgroundColor: '#C6C6C6',
        borderRadius: 10,
        padding: 10,
        borderWidth: 1,
        borderColor: '#E9E9E9',
        justifyContent: 'center', alignItems: 'center',
        alignContent: 'center',
        textAlign: 'center',
    },
    coltom1: {
        width: '90%',
        marginLeft: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
        fontSize: 16,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#fff',
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: "#333",
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        borderColor: '#7e7e7e',
        elevation: 3,
    },
    itemflat: {
        fontSize: 16,
        flexDirection: 'column',
        backgroundColor: '#fff',
        padding: 20,
        height: 75,
        shadowColor: "#333",
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        borderColor: '#7e7e7e',
        elevation: 1,
    },
    ModalCont2: {
        flex: 1,
        backgroundColor: '#00000079',
    },
    wrap: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height * 0.25 // 25% window
    },
    itemText: {
        textAlign: 'justify',
        marginLeft: 10,
        fontSize: 12,
        width: '35%',
        height: 43,
    },
    btnSimpanDark: {
        flexDirection: 'row',
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        width: 150, height: 50,
        backgroundColor: '#87cefa',
        borderRadius: 10,
        padding: 10,
        borderWidth: 1,
        borderColor: '#E9E9E9',
        justifyContent: 'center', alignContent: 'center',
        marginLeft: 120,
        fontSize: 12,
    },
    txtDesc: {
        color: '#2E3E5C',
        fontSize: 15,
        paddingLeft: 5,
        height: 100,
        marginHorizontal: 15,
    },
    infoContainer: {
        width: '90%',
        marginLeft: 30,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        height: 70,
        borderColor: '#E9E9E9',
    },
    currentStepText: {
        color: "#7e7e7e",
        fontSize: 12
    },
    kotak5: {
        width: '90%',
        height: 90,
        borderRadius: 10,
        borderWidth: 1,
        marginLeft: 20,
        marginTop: 10,
        borderColor: '#E9E9E9',
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    Label2: {
        marginBottom: -30,
        marginLeft: 25,
        color: '#000',
    },
    kotak3: {
        color: '#000000',
        marginTop: 2,
        marginLeft: 20,
        marginRight: 10,
        borderRadius: 2,
        borderWidth: 0.1,
        fontSize: 12,
        height: 40,
        width: '90%',
        backgroundColor: '#F0F8FF',
    },
    kotak4: {
        color: '#000000',
        marginTop: 2,
        marginLeft: 18,
        marginRight: 10,
        borderRadius: 2,
        borderWidth: 0.1,
        fontSize: 12,
        height: 50,
        width: '90%',
        backgroundColor: '#F0F8FF',
    },
    kotak6: {
        width: '90%',
        height: 100,
        borderRadius: 10,
        borderWidth: 1,
        marginLeft: 20,
        marginTop: 10,
        borderColor: '#E9E9E9',
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
});