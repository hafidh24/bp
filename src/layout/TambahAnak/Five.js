import { ScrollView, Text, View, StyleSheet, Dimensions, TextInput, TouchableOpacity, ToastAndroid, Image, Modal } from 'react-native'
import React, { Component } from 'react'
import { Picker } from '@react-native-picker/picker';
import DatePicker from 'react-native-date-picker';
import { x, date } from '../../assets/images'
export class Five extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nikibu: '',
            namaibu: '',
            agamaibu: '',
            temibu: '',
            alamatibu: '',
            dateibu: '',
            dateibu2: '',
            provibu: '',
            kabibu: '',
            kecibu: '',
            kelibu: '',
            penghasilanibu: '',
            penyebabibu: '',
            prov: [],
            kab: [],
            kecamatan: [],
            kelurahan: [],
            penghasilan: [],
            gaji: '',
            deskripsi: '',
            totalSteps: "",
            currentStep: "",
            nameError: '',

            // so: props.route.params.so,
            enabled: true,//props.route.params.so
            dateibu: new Date(),
            dateibu2: new Date(),
            // date2: new Date(),
            modaldate: false,
            modaldate2: false,
            show: false,
            isVisible: false,
        }
    }
    // static getDerivedStateFromProps = props => {
    //     const { getTotalSteps, getCurrentStep } = props;
    //     return {
    //         totalSteps: getTotalSteps(),
    //         currentStep: getCurrentStep()
    //     };
    // };

    // nextStep = () => {
    //     const { next, saveState } = this.props;
    //     saveState({
    //         NIK: '',
    //         ibu: '',
    //         agama: [],
    //         ag: '',
    //         lahir: '',
    //         tglahir: '',
    //         tgmati: '',
    //         alamat: '',
    //         prov: [],
    //         pro: '',
    //         kabupaten: [],
    //         kab: '',
    //         kecamatan: [],
    //         kec: '',
    //         kelurahan: [],
    //         kel: '',
    //         penghasilan: [],
    //         gaji: '',
    //         deskripsi: '',
    //         totalSteps: "",
    //         currentStep: "",
    //     });
    //     next();
    // };
    componentDidMount() {
        this.GetkotaAPi();
        this.GetprovAPi();
    }
    GetprovAPi() {
        fetch('https://berbagibahagia.org/api/getprov').then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                prov: resdata.data

            })
        })
    }
    GetkotaAPi() {
        fetch('https://berbagibahagia.org/api/getkota/' + this.state.pro).then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                kota: resdata.data

            })
        })
    }
    render() {
        const { currentStep, totalSteps } = this.state;

        return (
         <ScrollView  style={{ backgroundColor: '#fff' }}>
            <Text style={style.Label2}>Informasi Ibu</Text>
            <View style={{ height: '100%', alignContent: 'center', alignSelf: 'center' }}>
                    <TextInput
                        style={style.kotak3}
                        editable={this.props.route.params.SOT === 'Yatim_Piatu' ? false : true && this.props.route.params.SOT === 'Piatu' ? false : true}
                        onChangeText={nikibu => this.setState({ nikibu })}
                        value={this.state.nikibu}
                        placeholder="NIK Ibu"
                        placeholderTextColor="#C0C0C0"
                    />
                    <TextInput
                        style={style.kotak3}
                        onChangeText={namaayah => this.setState({ namaibu })}
                        value={this.state.namaibu}
                        placeholder="Nama Ibu"
                        placeholderTextColor="#C0C0C0"
                    />
                    <View
                        style={style.kotakpicker}>
                        <Picker
                            style={style.Textinputcss}
                            selectedValue={this.state.agamaibu}
                            onValueChange={itemValue =>
                                this.setState({ agamaibu: itemValue, show: 1 })
                            }>
                            <Picker.Item
                                style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                label="Agama"
                                value=""
                            />
                            <Picker.Item
                                style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                label="Islam"
                                value="Islam"
                            />
                            <Picker.Item
                                style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                label="Kristen"
                                value="Kristen"
                            />
                            <Picker.Item
                                style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                label="Hindu"
                                value="Hindu"
                            />
                            <Picker.Item
                                style={{ fontSize: 14, fontFamily: 'Poppins-Regular' }}
                                label="Budha"
                                value="Budha"
                            />
                        </Picker>
                    </View>

                    <TextInput
                        style={style.kotak3}
                        onChangeText={temibu => this.setState({ temibu })}
                        value={this.state.temibu}
                        placeholder="Tempat Lahir"
                        placeholderTextColor="#C0C0C0"
                    />
                    <View style={{ flexDirection: 'row' }}>
                    <Text
                            style={style.kotak7}>
                            {this.state.dateibu.toLocaleString('default', { month: 'long' })}
                        </Text>
                        <View
                            style={{
                                borderColor: '#DDD',
                                borderWidth: 1,
                                height: 50,
                                width: 50,
                                borderRadius: 10,
                                top: 10,
                                marginLeft: 10,

                            }}>
                            <TouchableOpacity TouchableOpacity onPress={() => this.setState({ modaldate: true })}>
                                <View style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 10, }}>
                                    {/* <Date /> */}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Modal
                        animationType={"slide"}
                        transparent={true}
                        visible={this.state.modaldate}
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',

                        }}>
                        <View style={style.ModalCont2}>
                            <View style={{
                                paddingTop: 5,
                                backgroundColor: '#ffffff',
                                // flexDirection: 'row',
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                                height: '27%',
                                shadowColor: "#333",
                                shadowOffset: {
                                    width: 1,
                                    height: 1,
                                },
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                                elevation: 3,
                                alignItems: 'center',
                                position: 'absolute',
                                bottom: 0,
                                left: 0,
                                right: 0,
                            }}>
                                <Text style={style.tglText}>Pilih Tanggal</Text>
                                <ScrollView style={{ width: '100%', height: '100%' }}>

                                    <TouchableOpacity onPress={() => this.setState({ modaldate: false })} style={{ position: 'absolute', right: 20, top: 20 }}>
                                        <Image source={x}
                                            style={{
                                                height: 30,
                                                width: 30, alignItems: 'center',
                                            }}></Image>
                                    </TouchableOpacity>
                                    <Modal
                        animationType={"slide"}
                        transparent={true}
                        visible={this.state.modaldate}
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',

                        }}>
                        <View style={style.ModalCont2}>
                            <View style={{
                                paddingTop: 5,
                                backgroundColor: '#ffffff',
                                // flexDirection: 'row',
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                                height: '27%',
                                shadowColor: "#333",
                                shadowOffset: {
                                    width: 1,
                                    height: 1,
                                },
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                                elevation: 3,
                                alignItems: 'center',
                                position: 'absolute',
                                bottom: 0,
                                left: 0,
                                right: 0,
                            }}>
                                <Text style={style.tglText}>Pilih Tanggal</Text>
                                <ScrollView style={{ width: '100%', height: '100%' }}>

                                    <TouchableOpacity onPress={() => this.setState({ modaldate: false })} style={{ position: 'absolute', right: 20, top: 20 }}>
                                        <Image source={x}
                                            style={{
                                                height: 30,
                                                width: 30, alignItems: 'center',
                                            }}></Image>
                                    </TouchableOpacity>
                                    <DatePicker
                                        date={this.state.dateibu}
                                        placeholder="select date"
                                        onDateChange={(dateibu) =>
                                            this.setState({ dateibu }, () => console.log(this.state.dateibu))
                                        }
                                        androidVariant="nativeAndroid"
                                        mode='date'

                                    />
                                </ScrollView>
                            </View>
                        </View>
                    </Modal>
                                </ScrollView>
                            </View>
                        </View>
                    </Modal>
                    </View>
                    <View>
                        <View style={style.infoContainer} >
                            <TextInput style={style.txtDesc} placeholder="Alamat Lengkap"
                                placeholderTextColor='#A9A9A9'
                                keyboardType="email-address"
                                value={this.state.alamatayah}
                                onChangeText={alamatibu => this.setState({ alamatibu })}
                                multiline={true}
                                numberOfLines={5}
                                autoCorrect={false}>
                            </TextInput>
                        </View>
                    </View>
                    <View
                        style={style.kotakpicker}>
                        <Picker
                            enabled={this.props.route.params.SOT === 'Yatim_Piatu' ? false : true && this.props.route.params.SOT === 'Piatu' ? false : true}
                            selectedValue={this.state.provibu}
                            onValueChange={(itemValue) => {
                                this.setState({
                                    provibu: itemValue
                                })
                            }}>
                            <Picker.Item style={{ fontSize: 12 }} label={'Pilih Provinsi'} value={'0'} key={'0'} />

                            {
                                this.state.prov.map((prov) =>
                                    <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={prov.nama} value={prov.id_prov} key={prov.id_prov} />
                                )}
                        </Picker>
                    </View>

                    <View
                        style={style.kotakpicker}>
                        <Picker
                            enabled={this.props.route.params.SOT === 'Yatim_Piatu' ? false : true && this.props.route.params.SOT === 'Piatu' ? false : true}
                            selectedValue={this.state.kabibu}
                            onFocus={() => { this.GetKabupatenAPi() }}
                            onValueChange={(itemValue, prov) => {
                                this.setState({
                                    kabibu: itemValue
                                })
                            }}>
                            <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kabupaten'} value={'0'} key={'0'} />

                            {
                                this.state.kab.map((kab) =>
                                    <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kab.nama.toString()} value={kab.id_kab} key={kab.id_kab} />
                                )}
                        </Picker>
                    </View>
                    <View
                        style={style.kotakpicker}>
                        <Picker
                            enabled={this.props.route.params.SOT === 'Yatim_Piatu' ? false : true && this.props.route.params.SOT === 'Piatu' ? false : true}
                            selectedValue={this.state.kecibu}
                            onFocus={() => { this.GetKecamatanAPi() }}
                            onValueChange={(itemValue, kab) => {
                                this.setState({
                                    kecibu: (itemValue)
                                })
                            }}>
                            <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kecamatan'} value={'0'} key={'0'} />

                            {
                                this.state.kecamatan.map((kec) =>
                                    <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kec.nama.toString()} value={kec.id_kec} key={kec.id_kec} />
                                )}
                        </Picker>
                    </View>
                    <View
                        style={style.kotakpicker}>
                        <Picker
                            enabled={this.props.route.params.SOT === 'Yatim_Piatu' ? false : true && this.props.route.params.SOT === 'Piatu' ? false : true}
                            selectedValue={this.state.kelibu}
                            onFocus={() => { this.GetKelurahanAPi() }}
                            onValueChange={(itemValue, kec) => {
                                this.setState({
                                    kelibu: (itemValue)
                                })
                            }}>
                            <Picker.Item style={{ fontSize: 12 }} label={'Pilih Kelurahan'} value={'0'} key={'0'} />
                            {
                                this.state.kelurahan.map((kel) =>
                                    <Picker.Item style={{ height: '100%', width: '100%', fontSize: 12, }} label={kel.nama.toString()} value={kel.id_kel} key={kel.id_kel} />
                                )}
                        </Picker>
                    </View>

                    <View style={style.kotakpicker}>
                        <Picker style={style.Textinputcss} mode="dropdown"
                            selectedValue={this.state.penghasilanibu}
                            enabled={this.props.route.params.SOT === 'Yatim_Piatu' ? false : true && this.state.SOT === 'Piatu' ? false : true}
                            onValueChange={(itemValue,) => {
                                this.setState({
                                    penghasilanibu: itemValue
                                })
                            }}>
                            <Picker.Item style={{ fontSize: 12 }} label={'Pilih Penghasilan'} value={'0'} key={'0'} />
                            <Picker.Item label="Dibawah Rp.500.000,-" value="Kakak" />
                            <Picker.Item label="Rp.500.000,- s/d Rp.1.500.000,-" value="1" />
                            <Picker.Item label="Rp.1.500.000,- s/d Rp.2.500.000,-" value="2" />
                            <Picker.Item label="Rp.2.500.000,- s/d Rp.3.500.000,-" value="3" />
                            <Picker.Item label="Rp.3.500.000,- s/d Rp.5.000.000,-" value="4" />
                            <Picker.Item label="Rp.5.000.000,- s/d Rp.7.000.000,-" value="5" />
                            <Picker.Item label="Rp.7.000.000,- s/d Rp.10.000.000,-" value="6" />
                            <Picker.Item label="Diatas Rp.10.000.000,-" value="7" />
                        </Picker>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                    <Text
                            style={style.kotak7}>
                            {this.state.dateibu.toLocaleString('default', { month: 'long' })}
                        </Text>
                        <View
                            style={{
                                borderColor: '#DDD',
                                borderWidth: 1,
                                height: 50,
                                width: 50,
                                borderRadius: 10,
                                top: 10,
                                marginLeft: 10,

                            }}>
                            <TouchableOpacity TouchableOpacity onPress={() => this.setState({ modaldate2: true })}>
                                <View style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 10, }}>
                                    {/* <Date /> */}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <Modal
                        animationType={"slide"}
                        transparent={true}
                        visible={this.state.modaldate2}
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',

                        }}>
                        <View style={style.ModalCont2}>
                            <View style={{
                                paddingTop: 5,
                                backgroundColor: '#ffffff',
                                // flexDirection: 'row',
                                borderTopLeftRadius: 10,
                                borderTopRightRadius: 10,
                                height: '27%',
                                shadowColor: "#333",
                                shadowOffset: {
                                    width: 1,
                                    height: 1,
                                },
                                shadowOpacity: 0.3,
                                shadowRadius: 2,
                                elevation: 3,
                                alignItems: 'center',
                                position: 'absolute',
                                bottom: 0,
                                left: 0,
                                right: 0,
                            }}>
                                <Text style={style.tglText}>Pilih Tanggal</Text>
                                <ScrollView style={{ width: '100%', height: '100%' }}>

                                    <TouchableOpacity onPress={() => this.setState({ modaldate2: false })} style={{ position: 'absolute', right: 20, top: 20 }}>
                                        <Image source={x}
                                            style={{
                                                height: 30,
                                                width: 30, alignItems: 'center',
                                            }}></Image>
                                    </TouchableOpacity>
                                    <DatePicker
                                        date={this.state.dateibu2}
                                        placeholder="select date"
                                        onDateChange={(dateibu2) =>
                                            this.setState({ dateibu2 }, () => console.log(this.state.dateibu2))
                                        }
                                        androidVariant="nativeAndroid"
                                        mode='date'

                                    />
                                </ScrollView>
                            </View>
                        </View>
                    </Modal>
                    </View>
                    <TextInput
                        style={style.kotak3}
                        onChangeText={text => this.setState({ text })}
                        value={this.state.penyebabayah}
                        placeholder="Penyebab Kematian"
                        keyboardType='numberic'
                        placeholderTextColor="#C0C0C0"
                    />
                    <TouchableOpacity style={style.nextbuttonTextStyle} onPress={() =>
                        // this.state.anak === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //     this.state.dari === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //         this.state.bina === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //             this.state.jarak === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                 this.state.nickname === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                     this.state.full === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                         this.state.ag === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                             this.state.lahir === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                                 this.state.sm === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                                     this.state.tglahir === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                                         this.state.gender === '' ? alert('Tolong ada kolom yang belum terisi') :
                        //                                             this.state.kendaraan === '' ? alert('Tolong ada kolom yang belum terisi') :
                        this.props.navigation.replace('Six', {
                            nikibu: '',
            namaibu: '',
            agamaibu: '',
            temibu: '',
            alamatibu: '',
            dateibu: '',
            dateibu2: '',
            provibu: '',
            kabibu: '',
            kecibu: '',
            kelibu: '',
            penghasilanibu: '',
            penyebabibu: '',
                            nikayah: this.props.route.params.nikayah,
                            namaayah: this.props.route.params.namaayah,
                            agamaayah: this.props.route.params.agamaayah,
                            temayah: this.props.route.params.temayah,
                            alamatayah: this.props.route.params.alamatayah,
                            dateayah: this.props.route.params.dateayah,
                            dateayah2: this.props.route.params.dateayah2,
                            provayah: this.props.route.params.provayah,
                            kabayah: this.props.route.params.kabayah,
                            kecayah: this.props.route.params.kecayah,
                            kelayah: this.props.route.params.kelayah,
                            penghasilanayah: this.state.penghasilanayah,
                            penyebabayah: this.state.penyebabayah,
                            nikanak: this.props.route.params.nikanak,
                            anak: this.props.route.params.anak,
                            saudara: this.props.route.params.saudara,
                            panggilan: this.props.route.params.panggilan,
                            namaanak: this.props.route.params.namaanak,
                            agama: this.props.route.params.agama,
                            tempatlahir: this.props.route.params.tempatlahir,
                            dateanak: this.props.route.params.dateanak,
                            JK: this.props.route.params.JK,
                            TB: this.props.route.params.TB,
                            kendaraan: this.props.route.params.kendaraan,
                            pelfa: this.props.route.params.pelfa,
                            hobi: this.props.route.params.hobi,
                            JB: this.props.route.params.JB,
                            kelas: this.props.route.params.kelas,
                            namasek: this.props.route.params.namasek,
                            alamatsek: this.props.route.params.alamatsek,
                            semester: this.props.route.params.semester,
                            jurusan: this.props.route.params.jurusan,
                            kepala: this.props.route.params.kepala,
                            KK: this.props.route.params.KK,
                            cabang: this.props.route.params.cabang,
                            binaan: this.props.route.params.binaan,
                            shel: this.props.route.params.shel,
                            SOT: this.props.route.params.SOT,
                            namabank: this.props.route.params.namabank,
                            norek: this.props.route.params.norek,
                            an_rek: this.props.route.params.an_rek,
                            nohp: this.props.route.params.nohp,
                            an_hp: this.props.route.params.an_hp,
                            notelp: this.props.route.params.notelp,
                            surket: this.props.route.params.surket,
                            sktm: this.props.route.params.sktm,
                        })} >
                        <Text style={{ justifyContent: 'center', textAlign: 'center', color: '#fff' }}>Lanjutkan</Text>
                    </TouchableOpacity>
                </View>
         </ScrollView>
        )
    }
}

export default Five
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const style = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: windowWidth * 0.95,
        height: windowHeight * 0.29,
        flexDirection: 'row',
    },
    header2: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: windowWidth * 0.6,
        height: windowHeight * 0.3,
        flexDirection: 'row',
    },
    contentContainer: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
        flex: 1,
    }, // vildan menghapus flex dan menambahkan backgroundCOlor,width dan hight
    colnilai: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    Label2: {
        marginTop: -5,
        fontSize: 16,
        marginTop: 10,
        marginLeft: 10,
        width: '100%',
        color: '#000',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
    },
    Label3: {
        marginTop: 10,
        fontSize: 16,
        width: '100%',
        color: '#000',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
    },
    Label3: {
        marginTop: 15,
        fontSize: 16,
        width: '100%',
        color: '#000',
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: 'Poppins-Medium',
    },
    title2: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginRight: 20,
        fontSize: 15,
        color: '#000',
        fontFamily: 'Poppins-SemiBold',
    },
    Textinputcss: {
        color: '#C0C0C0',
        marginTop: -10,
        borderRadius: 10,
        borderWidth: 1,
        fontSize: 10,
        height: 40,
        borderColor: '#C0C0C0',
        fontFamily: 'Poppins-Regular',
    },
    Textinputcss2: {
        color: '#C0C0C0',
        marginTop: 10,
        left: 2,
        marginLeft: 10,
        borderRadius: 10,
        borderWidth: 1,
        fontSize: 10,
        height: 30,
        width: windowWidth - 240,
        borderColor: '#C0C0C0',
        fontFamily: 'Poppins-Regular',
    },
    kotak3: {
        marginTop: 10,
        color: '#000',
        borderRadius: 10,
        borderWidth: 0.1,
        fontSize: 13,
        height: 50,
        width: windowWidth - 40,
        padding: 12,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        fontFamily: 'Poppins-Regular',
    },
    kotak4: {
        marginTop: 10,
        color: '#000',
        borderRadius: 10,
        borderWidth: 0.1,
        fontSize: 13,
        height: 50,
        width: windowWidth - 123,
        padding: 12,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        fontFamily: 'Poppins-Regular',
    },
    kotak7: {
        marginTop: 10,
        color: "#C0C0C0",
        borderRadius: 10,
        borderWidth: 0.1,
        fontSize: 13,
        height: 50,
        width: windowWidth - 130,
        padding: 12,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        fontFamily: 'Poppins-Regular',
    },
    kotak5: {
        marginTop: 15,
        color: '#000',
        borderRadius: 10,
        borderWidth: 0.1,
        fontSize: 13,
        height: 100,
        width: windowWidth - 40,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 12,
        fontFamily: 'Poppins-Regular',
        textAlignVertical: 'top'
    },
    kotak6: {
        marginTop: 10,
        color: '#000',
        borderRadius: 10,
        marginHorizontal: 10,
        borderWidth: 0.1,
        fontSize: 14,
        height: 50,
        width: 83,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        padding: 12,
        fontFamily: 'Poppins-Regular',
        textAlignVertical: 'top'
    },
    kotakpicker: {
        marginTop: 10,
        paddingVertical: 5,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#DDD',
        width: windowWidth - 20,
    },
    infoContainer: {
        width: '100%',
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        height: 70,
        borderColor: '#DDDDDD',
        backgroundColor: '#fff',
    },
    ModalCont2: {
        flex: 1,
        backgroundColor: '#00000079',
    },
    imgSmall: {
        position: 'absolute', flex: 1, alignItems: 'center', justifyContent: 'center'
    },
    nextbuttonTextStyle: {
        backgroundColor: '#00A9B8',
        height: 50,
        width: 130,
        marginTop: 10,
        borderRadius: 12,
        color: 'white',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        paddingHorizontal: 25,
        paddingVertical: 13,
        fontFamily: 'Poppins-SemiMedium',
        fontSize: 15,
    },
});