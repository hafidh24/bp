import { ScrollView, Text, View, StyleSheet, Dimensions } from 'react-native'
import React, { Component } from 'react'

export class Oprasional extends Component {
  render() {
    return (
      <ScrollView style={{ backgroundColor: '#fff', flex: 1 }}>
        <Text style={{ textAlign: 'center', fontSize: 14, marginTop: 5 }}>Pengajuan Biaya Oprasional</Text>
        <View style={{ marginLeft: 25, }}>
          <TouchableOpacity style={style.btntambah} onPress={() => this.SaveData()}>
            <Text style={{ color: '#fff' }}>Simpan</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  kotak3: {
    marginTop: 10,
    color: '#000',
    borderRadius: 10,
    borderWidth: 0.1,
    fontSize: 13,
    height: 50,
    marginLeft: 20,
    width: windowWidth - 40,
    padding: 12,
    backgroundColor: '#fff',
    borderColor: '#DDDDDD',
    borderWidth: 1,
    fontFamily: 'Poppins-Regular',
  },
  itemflat: {
    flex: 1,
    fontSize: 12,
    flexDirection: 'row',
    marginLeft: 10,
    paddingRight: 30,
    backgroundColor: '#fff',
    color: '#000',
    marginVertical: 10,
    marginHorizontal: 16,
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderRadius: 15,
  },
  btntambah: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    height: 50,
    borderWidth: 1,
    borderRadius: 10,
    color: '#fff',
    borderColor: '#00A9B8',
    backgroundColor: '#00A9B8',

  }
});
export default Oprasional