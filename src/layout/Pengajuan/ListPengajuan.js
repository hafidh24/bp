import { Text, View, FlatList, StyleSheet, TouchableOpacity, Modal, SafeAreaView } from 'react-native'
import React, { Component } from 'react'
import {
  Kidsabu,
  Close,
} from '../../assets/icons'
import { connect } from 'react-redux';
export class ListPengajuan extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: [],
      Pilih: false,

    }
  }
  componentDidMount() {
    this.GetPengajuanAPi();

  }
  GetPengajuanAPi() {
    fetch('https://kilauindonesia.org/datakilau/api/tutor').then(res => {
      if (res.status === 200)
        return res.json()
    }).then(resdata => {
      console.log(resdata.data)
      this.setState({
        list: resdata.data,
        refreshing: false,

      })
    })
  }

  render() {
    return (
      <View>
        {
          this.props.user.presensi === 'karyawan' ? //admin cabang//
            <View style={{ backgroundColor: '#fff', height: '100%' }}>
              <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 16, marginTop: 15, marginBottom: 15 }}>Riwayat Pengajuan</Text>
              <FlatList
                pagingEnabled={true}
                data={this.state.list}
                renderItem={({ item }) => (
                  // style={style.kotakbaru4}
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailPenga')}>
                    <View style={style.kotakstat}>
                      <Text style={[style.status]}>Pending</Text>
                      <Text style={style.judul}>Pengajuan Beasiswa Pendidikan</Text>
                      <View style={{ flexDirection: 'row', marginTop: 10, }}>
                        <Kidsabu></Kidsabu>
                        <Text style={{ marginTop: 6, fontSize: 16, color: '#bdbdbd', fontWeight: 'bold' }}>40 Anak</Text>
                      </View>
                      <Text style={{ textAlign: 'right', color: '#bdbdbd', fontWeight: 'bold' }}>12 Jan 2022</Text>
                    </View>
                  </TouchableOpacity>
                )}>
              </FlatList>
            </View>
            :
            <View />
        }
        {
          this.props.user.presensi === 'admin' ? //Pengelola//
            <View style={{ backgroundColor: '#fff', height: '100%' }}>
              <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 16, marginTop: 15, marginBottom: 15 }}>Riwayat Pengajuan</Text>
              <FlatList
                pagingEnabled={true}
                data={this.state.list}
                renderItem={({ item }) => (
                  // style={style.kotakbaru4}
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailPenga')}>
                    <View style={style.kotakstat}>
                      <Text style={[style.status]}>Pending</Text>
                      <Text style={style.judul}>Pengajuan Beasiswa Pendidikan</Text>
                      <View style={{ flexDirection: 'row', marginTop: 10, }}>
                        <Kidsabu></Kidsabu>
                        <Text style={{ marginTop: 6, fontSize: 16, color: '#bdbdbd', fontWeight: 'bold' }}>40 Anak</Text>
                      </View>
                      <Text style={{ textAlign: 'right', color: '#bdbdbd', fontWeight: 'bold' }}>12 Jan 2022</Text>
                    </View>
                  </TouchableOpacity>
                )}>
              </FlatList>
              <View style={{ flexDirection: 'row', marginVertical: 10, justifyContent: 'space-around', alignItems: 'center', }}>
                <TouchableOpacity style={style.btnkembali} onPress={() => this.props.navigation.navigate('Home')}>
                  <Text>Kembali</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.btntambah} onPress={() => this.setState({ Pilih: true })}>
                  <Text style={{ color: '#fff' }}>+ Tambah Pengajuan</Text>
                </TouchableOpacity>
              </View>

              <Modal animationType={"fade"} transparent={true}
                visible={this.state.Pilih}
                onRequestClose={() => this.setState({ Pilih: false })}>

                <SafeAreaView style={style.containerSafe}>
                  <TouchableOpacity activeOpacity={1.0} onPress={() => this.setState({ Pilih: false })} style={style.ModalCont}>
                    <View style={{
                      paddingTop: 5,
                      marginHorizontal: 10,
                      backgroundColor: '#fff',
                      // flexDirection: 'row',
                      borderRadius: 20,
                      height: 250,
                      shadowColor: "#333",
                      shadowOffset: {
                        width: 1,
                        height: 1,
                      },
                      shadowOpacity: 0.3,
                      shadowRadius: 2,
                      elevation: 3,
                      alignItems: 'center'
                    }}>
                      <TouchableOpacity onPress={() => this.setState({ Pilih: false })} style={{ position: 'absolute', right: 20, top: 20 }}>
                        <Close />
                      </TouchableOpacity>
                      <Text style={style.txtPresensi}>Pilih Pengajuan</Text>
                      <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <View style={[style.kotakkecil, { backgroundColor: '#00A9B8' }]}>
                          <TouchableOpacity onPress={() => this.props.navigation.navigate('Pengajuan', this.setState({ Pilih: false }))}>
                            {/* <KKada style={{ justifyContent: 'center', alignSelf: 'center' }} /> */}
                            <Text style={{ color: '#fff', textAlign: 'center', padding: 5, fontWeight: 'bold' }}>Ajukan Pengajuan Anak</Text>
                          </TouchableOpacity>
                        </View>

                        <View style={[style.kotakkecil, { backgroundColor: '#00A9B8', }]}>
                          <TouchableOpacity onPress={() => this.props.navigation.navigate('Oprasional', this.setState({ Pilih: false }))}>
                            {/* <KKada style={{ justifyContent: 'center', alignSelf: 'center' }} /> */}
                            <Text style={{ color: '#fff', textAlign: 'center', padding: 5, fontWeight: 'bold' }}>Ajukan Biaya Operasional</Text>
                          </TouchableOpacity>
                        </View>
                      </View>

                      <View style={{ marginTop: '5%', alignItems: 'center', flexDirection: 'row', alignContent: 'center', }}>
                        <TouchableOpacity onPress={() => this.setState({ Pilih: false }) + ToastAndroid.show('Batal', ToastAndroid.SHORT)}>
                          <View style={{ height: 50, borderColor: '#00A9B8', borderWidth: 1, width: 100, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', borderRadius: 5 }}>
                            <Text style={{ color: '#00A9B8', fontFamily: 'Poppins-Medium', fontSize: 14 }}>Batal</Text>
                          </View>
                        </TouchableOpacity>

                      </View>
                    </View>
                  </TouchableOpacity>
                </SafeAreaView>
              </Modal>
            </View>



            :
            <View />
        }
      </View>
    )
  }
}

const style = StyleSheet.create({
  contentContainer: {
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
  },
  title1: {
    marginRight: 20,
    marginLeft: 20,
    marginTop: 15,
    marginBottom: 15,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
  },
  label: {
    marginTop: 20,
    marginLeft: 10,
  },
  label3: {
    marginTop: 20,
    marginLeft: 30,
  },
  label2: {
    marginLeft: -40,
  },
  kotakbaru1: {
    width: '95%',
    height: 200,
    borderRadius: 15,
    marginTop: 5,
    marginLeft: 10,
    borderWidth: 1,
    borderColor: '#E9E9E9',
    backgroundColor: '#fff',
    marginBottom: 10,
  },
  kotakstat: {
    backgroundColor: '#fff',
    height: 140,
    marginVertical: 8,
    marginLeft: 20,
    width: '90%',
    borderRadius: 10,
    paddingHorizontal: '4%',
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
  },
  status: {
    marginTop: 10,
    color: '#D39800',
    backgroundColor: '#bdbdbd',
    width: 60,
    height: 30,
    fontWeight: 'bold',
    padding: 3,
    textAlign: 'center',
    borderRadius: 5,
  },
  judul: {
    marginTop: 10,
    fontWeight: 'bold',
    fontSize: 16,
    width: '90%',
    color: '#000',
  },
  btnkembali: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: '40%',
    height: 50,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#00A9B8',
  },
  btntambah: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: '40%',
    height: 50,
    borderWidth: 1,
    borderRadius: 10,
    color: '#fff',
    borderColor: '#00A9B8',
    backgroundColor: '#00A9B8',
  },
  txtPresensi: {
    justifyContent: 'center', alignItems: 'center',
    fontSize: 18,
    marginTop: 20,
    fontWeight: 'bold',
    color: '#7e7e7e'
  },
  kotakkecil: {
    flexDirection: 'column',
    borderColor: '#bdbdbd',
    borderWidth: 1,
    width: '40%',
    height: 100,
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 10,
    marginHorizontal: 10,
  },
  containerSafe: {
    flex: 1,
    flexDirection: 'column',
  },
  ModalCont: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#00000099',
    paddingHorizontal: 10,
  },
})
const mapStateToProps = (state) => {
  return {
    user: state,
    initialState: state
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeUser: (data) => dispatch({ type: 'CHANGE/USER', payload: data }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListPengajuan);