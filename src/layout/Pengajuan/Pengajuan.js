import { Text, View, TextInput, StyleSheet, Dimensions, FlatList, TouchableOpacity, Image, Alert } from 'react-native'
import React, { Component } from 'react'
import { test, } from '../../assets/images';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { LocationsH, TingkatH, JenisH, IconCari } from '../../assets/icons';
export class Pengajuan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tujuan: '',
            anak: [],
            cari: '',
            filter_sta: [],
            seleksi: false,
            isSelected: false,
            select: [],


        };
    }
    componentDidMount() {
        this.GetPengajuanAPi();
        // this.dataanak();
        let arr = this.state.select.map((item, index) => {
            item.isSelected = false
            return { ...item }
        })
        this.setState({ select: arr })
        console.log("arr data ==>", arr)


    }
    GetPengajuanAPi() {
        var seleksi = this.state
        fetch('https://kilauindonesia.org/datakilau/api/joindataanak').then(res => {
            if (res.status === 200)
                return res.json()
        }).then(resdata => {
            console.log(resdata.data)
            this.setState({
                anak: resdata.data,
                filter_sta: resdata.data,
                refreshing: false,

            })
        })
    }

    dataanak = async () => {
        try {
            const anakPB = await AsyncStorage.getItem('select');
            anakPB = JSON.parse(anakPB);
            if (anakPB !== null) {
                this.setState({ select: anakPB });
            }
            console.log('data berhasil di get', anakPB)
        } catch (e) {
            console.log('Done.')
        }
    }

    SaveData = async (select) => {
        try {
            await AsyncStorage.setItem('select', JSON.stringify(select),
            );
            console.log('sayang lifi');
        } catch (e) {
            console.log(e);
        }
    }

    selectanak = (id_anak) => {
        // if (id_anak == index) {
        //     item.isSelected = !item.isSelected;
        //     Alert.alert('Silahkan Pilih Anak yang inign di Cairkan Dananya');
        // } else {
        var select = this.state.select;
        var del = 0;
        for (let i = 0; i < this.state.select.length; i++) {
            if (this.state.select[i].id_anak === id_anak) {
                this.state.select.splice(i, 1);
                del = id_anak;
            }
        }
        if (id_anak != del) {
            select.push({ id_anak });
        }

        console.log(select);
        // this.setState({id_anak, no});

        // }

    }
    // selectanak = (id_anak,index) => {
    //    this.setState(prev => prev.select[])

    // }
    select11 = (full_name) => {
        const { filter_sta } = this.state;
        let arr = filter_sta.map((item, index) => {
            if (full_name == index) {
                item.isSelected = !item.isSelected;
            }
            return { ...item }
        })
        console.log("Selection ==>", full_name)
        this.setState({ filter_sta })
    }



    // selectanak = (id_anak,index) => {
    //     const {anak} = this.state;
    //     let arr = anak.map((item,index)=>{
    //         if(id_anak == index){
    //             item.isSelected = !item.isSelected;
    //         } else {
    //             var select = this.state.select;
    //             select.push({ id_anak });
    //             console.log(select);
    //             this.setState({ id_anak});

    //         }
    //         return{...item}
    //     })
    // }
    filterList(textToSearch) {
        this.setState({
            filter_sta: this.state.anak.filter(i => i.full_name.toLowerCase(textToSearch).includes(textToSearch)),
        });
    }
    render() {
        const filter_sta = this.state.anak.filter(item => item.status_cpb === 'PB')
        const lifi = [];
        for (let i = 0; i < this.state.seleksi; i++) {
            lifi.push(

            );
        }
        return (
            <View style={{ backgroundColor: '#fff', height: '100%' }}>
                <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 16, marginTop: 15, marginBottom: 15 }}>Pengajuan Dana</Text>
                <View>
                    <TextInput
                        style={style.kotak3}
                        onChangeText={tujuan => this.setState({ tujuan })}
                        value={this.state.tujuan}
                        keyboardType='default'
                        placeholder="Tujuan"
                        placeholderTextColor='#7e7e7e'
                    />
                </View>
                <Text style={{ marginLeft: 15 }}>List Anak Yang Diajukan(0)</Text>
                <View
                    style={[style.kotak3, { flexDirection: 'row' }]}>
                    <IconCari style={style.IconCari} name="your-icon" size={20} />
                    <TextInput
                        style={{
                            flexDirection: 'row',
                            backgroundColor: '#FFF',
                            paddingHorizontal: 40,
                            height: 38,
                            width: '90%',
                            borderRadius: 9,
                            marginTop: -9,
                        }}
                        color={'#000'}
                        onChangeText={(text) => {
                            this.filterList(text.toLowerCase()), this.setState({ cari: text })
                        }}
                        value={this.state.cari}
                        placeholder="Cari Anak Binaan"
                        placeholderTextColor="#C0C0C0"
                        underlineColorAndroid="transparent"
                    />
                </View>

                <FlatList
                    pagingEnabled={true}
                    data={filter_sta}
                    renderItem={({ item, index }) => (
                        // style={style.kotakbaru4}
                        <View >
                            <TouchableOpacity onPress={() => this.selectanak(item.id_anak)}>
                                <View style={[style.itemflat, {
                                    // borderWidth:this.state.select === item.id_anak ? 3 : 1,
                                    // borderColor:this.state.select === item.id_anak ? '#0076B8' : '#bdbdbb',
                                    borderWidth: this.state.select.find(data => data.id_anak === item.id_anak) ? 3 : 1,
                                    borderColor: this.state.select.find(data => data.id_anak === item.id_anak) ? '#0076B8' : '#bdbdbb' && item.id_anak === item.id_anak ? '#bdbdbb' : '#0076B8',
                                }]}>
                                    <View style={{
                                        width: '10%', justifyContent: 'center',

                                        backgroundColor: item.status_cpb === 'CPB' ? '#0076B8' : '#000' &&
                                            item.status_cpb === 'PB' ? '#00B855' : '#000' && item.status_cpb === 'NPB' ? '#E32845' : '#000' && item.status_cpb === 'BCPB' ? '#FFBB0C' : '#000'
                                    }}>
                                        <View style={{ width: '40%', justifyContent: 'center', alignItems: 'center', alignSelf: 'center', alignContent: 'center' }}>
                                            <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', color: '#fff' }}>{item.status_cpb}</Text>
                                        </View>
                                    </View>
                                    <View
                                        style={{
                                            height: 90,
                                            width: '100%',
                                            justifyContent: 'center',
                                        }}>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={test}
                                                    style={{
                                                        height: 50,
                                                        width: 50,
                                                        borderRadius: 45,
                                                        color: '#000',
                                                        marginRight: 30
                                                    }}
                                                />
                                                <View
                                                    style={{ flexDirection: 'column', marginLeft: '-10%', justifyContent: 'center', width: '70%' }}>
                                                    <Text style={{ color: '#000', fontFamily: 'Poppins-Medium', fontSize: 14, marginLeft: 10, }}>
                                                        {item.full_name}
                                                    </Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <JenisH style={{ marginLeft: 10 }} />
                                                            <Text style={{ color: '#000', fontSize: 10, marginLeft: 5, fontFamily: 'Poppins-Regular' }}>Non-Tahfidz</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row', }}>
                                                            <TingkatH style={{ marginLeft: 10, }} />
                                                            <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5 }}>Kelas {item.kelas}</Text>
                                                        </View>

                                                        <View style={{ flexDirection: 'row', }}>
                                                            <LocationsH style={{ marginLeft: 10, }} />
                                                            <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5 }}>{item.tempat_lahir}</Text>
                                                        </View>

                                                    </View >
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    )}>
                </FlatList>
                <View style={{ marginLeft: 25, }}>
                    <TouchableOpacity style={style.btntambah} onPress={() => this.SaveData()}>
                        <Text style={{ color: '#fff' }}>Simpan</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
    kotak3: {
        marginTop: 10,
        color: '#000',
        borderRadius: 10,
        borderWidth: 0.1,
        fontSize: 13,
        height: 50,
        marginLeft: 20,
        width: windowWidth - 40,
        padding: 12,
        backgroundColor: '#fff',
        borderColor: '#DDDDDD',
        borderWidth: 1,
        fontFamily: 'Poppins-Regular',
    },
    itemflat: {
        flex: 1,
        fontSize: 12,
        flexDirection: 'row',
        marginLeft: 10,
        paddingRight: 30,
        backgroundColor: '#fff',
        color: '#000',
        marginVertical: 10,
        marginHorizontal: 16,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderRadius: 15,
    },
    btntambah: {
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        height: 50,
        borderWidth: 1,
        borderRadius: 10,
        color: '#fff',
        borderColor: '#00A9B8',
        backgroundColor: '#00A9B8',

    }
});
export default Pengajuan