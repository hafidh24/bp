import { Text, View, Dimensions, FlatList, StyleSheet, TextInput, TouchableOpacity, Image } from 'react-native'
import React, { Component } from 'react'
import { test } from '../../assets/images';
import { LocationsH, TingkatH, JenisH, IconCari } from '../../assets/icons';
export class DetailPenga extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tujuan: '',
      anak: [],
      cari: '',
      filter_sta: [],

    };
  }
  componentDidMount() {
    this.GetPengajuanAPi();

  }
  GetPengajuanAPi() {
    fetch('https://kilauindonesia.org/datakilau/api/joindataanak').then(res => {
      if (res.status === 200)
        return res.json()
    }).then(resdata => {
      console.log(resdata.data)
      this.setState({
        anak: resdata.data,
        filter_sta: resdata.data,
        refreshing: false,

      })
    })
  }
  render() {
    const filter_sta = this.state.anak.filter(item => item.status_cpb === 'PB')

    return (
      <View style={{ backgroundColor: '#fff', height: '100%' }}>
        <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 16, marginTop: 15, marginBottom: 15 }}>Pengajuan Dana</Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ marginLeft: 15 }}>Jenis Pengajuan:</Text>
          <Text style={{ marginRight: 10 }}>Beasiswa Pendidikan</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ marginLeft: 15, marginTop: 5 }}>Status Pengajuan:</Text>
          <Text style={style.status}>Pending</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ marginLeft: 15 }}>Jumlah Anak Yang Diajukan:</Text>
          <Text style={{ marginRight: 30 }}>40</Text>
        </View>
        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={{ marginLeft: 15 }}>Jumlah Yang Dicairkan:</Text>
          <Text style={{ marginRight: 10 }}>Rp.10.000.000</Text>
        </View> */}
        <View
          style={[style.kotak3, { flexDirection: 'row' }]}>
          <IconCari style={style.IconCari} name="your-icon" size={20} />
          <TextInput
            style={{
              flexDirection: 'row',
              backgroundColor: '#FFF',
              paddingHorizontal: 40,
              height: 38,
              width: '90%',
              borderRadius: 9,
              marginTop: -9,
            }}
            color={'#000'}
            onChangeText={(text) => {
              this.filterList(text.toLowerCase()), this.setState({ cari: text })
            }}
            value={this.state.cari}
            placeholder="Cari Anak Binaan"
            placeholderTextColor="#C0C0C0"
            underlineColorAndroid="transparent"
          />
        </View>
        <View style={{
          height: 1,
          width: '100%',
          backgroundColor: '#bdbdbb',
          marginTop: 10
        }}></View>

        <FlatList
          pagingEnabled={true}
          data={filter_sta}
          renderItem={({ item }) => (
            // style={style.kotakbaru4}
            <View style={{}}>
              <TouchableOpacity>
                <View style={style.itemflat}>
                  <View style={{
                    width: '10%', justifyContent: 'center',
                    backgroundColor: item.status_cpb === 'CPB' ? '#0076B8' : '#000' &&
                      item.status_cpb === 'PB' ? '#00B855' : '#000' && item.status_cpb === 'NPB' ? '#E32845' : '#000' && item.status_cpb === 'BCPB' ? '#FFBB0C' : '#000'
                  }}>
                    <View style={{ width: '40%', justifyContent: 'center', alignItems: 'center', alignSelf: 'center', alignContent: 'center' }}>
                      <Text style={{ fontSize: 12, fontFamily: 'Poppins-Medium', color: '#fff' }}>{item.status_cpb}</Text>
                    </View>
                  </View>
                  <View
                    style={{
                      height: 90,
                      width: '100%',
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <View style={{ flexDirection: 'row' }}>
                        <Image source={test}
                          style={{
                            height: 50,
                            width: 50,
                            borderRadius: 45,
                            color: '#000',
                            marginRight: 30
                          }}
                        />
                        <View
                          style={{ flexDirection: 'column', marginLeft: '-10%', justifyContent: 'center', width: '70%' }}>
                          <Text style={{ color: '#000', fontFamily: 'Poppins-Medium', fontSize: 14, marginLeft: 10, }}>
                            {item.full_name}
                          </Text>
                          <View style={{ flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'row' }}>
                              <JenisH style={{ marginLeft: 10 }} />
                              <Text style={{ color: '#000', fontSize: 10, marginLeft: 5, fontFamily: 'Poppins-Regular' }}>Non-Tahfidz</Text>
                            </View>

                            <View style={{ flexDirection: 'row', }}>
                              <TingkatH style={{ marginLeft: 10, }} />
                              <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5 }}>Kelas {item.kelas}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', }}>
                              <LocationsH style={{ marginLeft: 10, }} />
                              <Text style={{ color: '#000', fontSize: 10, fontFamily: 'Poppins-Regular', marginLeft: 5 }}>{item.tempat_lahir}</Text>
                            </View>

                          </View>

                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}>
        </FlatList>

      </View>
    )
  }
}
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  kotak3: {
    marginTop: 10,
    color: '#000',
    borderRadius: 10,
    borderWidth: 0.1,
    fontSize: 13,
    height: 50,
    marginLeft: 20,
    width: windowWidth - 40,
    padding: 12,
    backgroundColor: '#fff',
    borderColor: '#DDDDDD',
    borderWidth: 1,
    fontFamily: 'Poppins-Regular',
  },
  itemflat: {
    flex: 1,
    fontSize: 12,
    flexDirection: 'row',
    marginLeft: 10,
    paddingRight: 30,
    backgroundColor: '#fff',
    color: '#000',
    marginVertical: 10,
    marginHorizontal: 16,
    shadowColor: '#858585',
    overflow: 'hidden',
    shadowRadius: 15,
    elevation: 6,
    shadowOpacity: '25%',
    borderColor: '#7e7e7e',
    borderRadius: 15,
  },
  btntambah: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    height: 50,
    borderWidth: 1,
    borderRadius: 10,
    color: '#fff',
    borderColor: '#00A9B8',
    backgroundColor: '#00A9B8',
  },
  status: {
    marginRight: 15,
    color: '#D39800',
    backgroundColor: '#bdbdbd',
    width: 60,
    height: 30,
    fontWeight: 'bold',
    padding: 3,
    textAlign: 'center',
    borderRadius: 5,
  },
});
export default DetailPenga