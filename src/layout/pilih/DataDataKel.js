import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import React, { Component } from 'react'

export class DataDataKel extends Component {
    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <View style={style.itemflat}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DataValidasi')}>
                        <Text style={style.labelkiri}>Data Validasi</Text>
                    </TouchableOpacity>
                </View>
                <View style={style.itemflat}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Keluarga')}>
                        <Text style={style.labelkiri}>List Keluarga</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const style = StyleSheet.create({
    itemflat: {
        fontSize: 12,
        backgroundColor: '#fff',
        color: '#000',
        marginVertical: 10,
        marginHorizontal: 16,
        shadowColor: '#858585',
        overflow: 'hidden',
        shadowRadius: 15,
        elevation: 6,
        shadowOpacity: '25%',
        borderColor: '#7e7e7e',
        borderRadius: 15,
        height: 60,
        width: '90%',
        justifyContent: 'center',
    },
    labelkiri: {
        fontSize: 12,
        fontWeight: 'bold',
        marginVertical: 5,
        marginLeft: 10,
    },
});
export default DataDataKel